# LNS_CMS repository

This repository contains 3 mainly folders:

* **instances**: This folder contains all 124 instances used in the experiment and one more instance used to calibrate and test some algorithms (aaa-fake-06). That instance is not intended to be used in experiments and its data is not included in any result.

* **source**: Contains the Java application's source code. The application in this repository is fully capable of reading .odem instance files and convert to Bunch instance format file (class dependant_class relation_weight). Also, the project can do the preprocessing at runtime (the pre-processing can reduce the graph size without changing the optimal MQ value). It also calculates the MQ value for each solution using the LNS metaheuristic.

* **data**: This folder contains the results obtained by the LNS algorithm. It has many other nested folders which will be explained as follows.

* **data/experiment_data**: Raw data obtained after the execution of any of the implemented algorithm. The main results, obtained after the execution of MIXED_1000 which is so called LNS_SMC (that algorithm led to better solutions than others tested). It has to folders with more data details. both folders start with **ALL\_FIXED\_18\_MIXED\_1000\_NO\_LIMIT\_**.

* **data/experiments_summary**: This folder has some structured auxiliary data and also an R script to read and process them.

* **data/LNS_SMC_best_solutions**: This folder contains all the best solution found by the LNS_CMS for each instance. IMPORTANT: The solution file may not contain all the original modules, because some of them may be pruned by the pre-processing algorithm.

* **data/LNS_SMC_modules_index_after_pre_processing**: This folder contains one file for each instance. Each file has the remaining modules names in the instance after the pre-processing. The files also have the module index. They are useful to recalculate the MQ given any solution.

* **data/other_experiments**: It has some spreadsheets with many data used to guide the study.

* **data/scripts**: Contains some R script files used to process the raw data files.