# Load data
rm(list=ls());

base_folder="D:/source_codes/unirio_lns_cms/data/experiments_summary/"; #desktop casa

ils_lns_data_file=paste(base_folder,"ils_lns_data.dat", sep="");
literature_data_file=paste(base_folder,"best_literature.dat", sep="");
instances_size_data_file=paste(base_folder,"instances_size.dat", sep="");

output_folder=paste(base_folder,"output_eng/", sep="");

boxplot_mq_file=paste(output_folder,"boxplot_mq_group_",sep="");
boxplot_time_file=paste(output_folder,"boxplot_time_group_",sep="");
boxplot_mq_12_file=paste(output_folder,"boxplot_12_mq",sep="");
boxplot_time_12_file=paste(output_folder,"boxplot_12_time",sep="");
tttplot_time_lns_ils_file=paste(output_folder,"tttplot_time_lns_ils",sep="");
tttplot_mq_lns_ils_file=paste(output_folder,"tttplot_mq_lns_ils",sep="");
boxplot_best_comp_=paste(output_folder,"boxplot_best_comp_",sep="");

boxplot_mq_rel_file=paste(output_folder,"boxplot_mq_rel_file",sep="");
boxplot_time_rel_file=paste(output_folder,"boxplot_time_rel_file",sep="");
boxplot_mq_rel_file_no_out=paste(output_folder,"boxplot_mq_rel_file_no_out",sep="");
boxplot_time_rel_file_no_out=paste(output_folder,"boxplot_time_rel_file_no_out",sep="");



ils_lns_data <- read.table(ils_lns_data_file, header=TRUE, sep=";");
best_literature_data <- read.table(literature_data_file, header=TRUE, sep=";");
instances_size_data <- read.table(instances_size_data_file, header=TRUE, sep=";");

# Separate instances
instances <- unique(ils_lns_data$INSTANCE);

# Calculates effect-size
vargha.delaney <- function(s1, s2) {
	(1 - (sum(rank(c(s1, s2))[seq_along(s1)]) / length(s1) - (length(s1) + 1) / 2) / length(s2)) * 100
}




#PASSO AUXILIAR - GERAR DADOS CONDENSADOS PARA OS RESULTADOS DO ALEXANDRE
result_ils <- matrix(nrow=length(instances), ncol=5, dimnames=list(instances, c("order","mq","mq_std","time_s","time_s_std")));
for (instancia_ in instances){
	ils_mq <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$MQ;
	ils_time <- (subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$TIME);#passar de milisegundos para segundo
	
	ils_mq <- sapply(sapply(ils_mq, as.character),as.numeric);
	ils_time <- sapply(sapply(ils_time, as.character),as.numeric);
	
	ils_time <- ils_time/1000;
	
	result_ils[instancia_, "order"] <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$ORDER[1];
	result_ils[instancia_, "mq"] <- mean(ils_mq);
	result_ils[instancia_, "mq_std"] <- sd(ils_mq);
	result_ils[instancia_, "time_s"] <- mean(ils_time);
	result_ils[instancia_, "time_s_std"] <- sd(ils_time);
}
round(result_ils,6);


#PASSO 0 (feito ao final do trabalho) - gerar min avg sd max (MQ e Tempo) apenas LNS
result = c();
# Create matrices to hold MQ values
result <- matrix(nrow=length(instances), ncol=9, dimnames=list(instances, c("order","minMQ","avgMQ", "sdMQ", "maxMQ", "minT","avgT", "sdT", "maxT")));
# Fill out matrices with data
#instancia_ <- instances[100];#debug

for (instancia_ in instances){
	order_ <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$ORDER[1];
	
	lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$MQ;
	lnsT <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$TIME;
	

	lns <- sapply(sapply(lns, as.character),as.numeric);
	lnsT <- sapply(sapply(lnsT, as.character),as.numeric);
	lnsT <- lnsT/1000;
	
	result[instancia_, "order"] <- order_;
	result[instancia_, "minMQ"] <- min(lns);
	result[instancia_, "avgMQ"] <- mean(lns);
	result[instancia_, "sdMQ"] <- sd(lns);
	result[instancia_, "maxMQ"] <- max(lns);
	
	result[instancia_, "minT"] <- min(lnsT);
	result[instancia_, "avgT"] <- mean(lnsT);
	result[instancia_, "sdT"] <- sd(lnsT);
	result[instancia_, "maxT"] <- max(lnsT);
}
round(result,6);






#PASSO 1 - GERAR TABELA COMPARATIVA ENTRE ILS E LNS

# Create matrices to hold MQ values
result <- matrix(nrow=length(instances), ncol=8, dimnames=list(instances, c("order","group","avglns", "sdlns", "avgils", "sdils", "pv1", "es1")));
# Fill out matrices with data
#instancia_ <- instances[100];#debug

#gerar pvalue e effect size lns vs ils
for (instancia_ in instances){
	group <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$GROUP[1];
	order_ <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$ORDER[1];
	
	ils <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$MQ;
	lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$MQ;
	

	ils <- sapply(sapply(ils, as.character),as.numeric);
	lns <- sapply(sapply(lns, as.character),as.numeric);

	pvalue <- wilcox.test(ils, lns)$p.value;
	es <- vargha.delaney(ils, lns);
	#pv2 <- wilcox.test(lns, um=lit)$p.value;
	#es2 <- length(subset(lns, lns >= lit));
	
	result[instancia_, "order"] <- order_;
	result[instancia_, "group"] <- group;
	result[instancia_, "avglns"] <- mean(lns);
	result[instancia_, "sdlns"] <- sd(lns);
	result[instancia_, "avgils"] <- mean(ils);
	result[instancia_, "sdils"] <- sd(ils);
	result[instancia_, "pv1"] <- pvalue;
	result[instancia_, "es1"] <- es/100;
}
round(result,6);







#PASSO 1.1 - GERAR TABELA COMPARATIVA ENTRE ILS E LNS - TEMPO

# Create matrices to hold MQ values
result_time <- matrix(nrow=length(instances), ncol=4, dimnames=list(instances, c("order","group","avglns", "avgils")));
# Fill out matrices with data
#instancia_ <- instances[100];#debug

#gerar pvalue e effect size lns vs ils
for (instancia_ in instances){
	group <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$GROUP[1];
	order_ <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$ORDER[1];
	
	ils <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$TIME;
	lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$TIME;
	

	ils <- sapply(sapply(ils, as.character),as.numeric);
	lns <- sapply(sapply(lns, as.character),as.numeric);
	
	ils <- ils/1000;
	lns <- lns/1000;

	pvalue <- wilcox.test(ils, lns)$p.value;
	es <- vargha.delaney(ils, lns);
	#pv2 <- wilcox.test(lns, um=lit)$p.value;
	#es2 <- length(subset(lns, lns >= lit));
	
	result_time[instancia_, "order"] <- order_;
	result_time[instancia_, "group"] <- group;
	result_time[instancia_, "avglns"] <- mean(lns);
	result_time[instancia_, "avgils"] <- mean(ils);
}
round(result_time,6);









#PASSO 2.1 E 2.2 - GERAR BOXPLOT. COM OS MQs E COM OS TEMPOS DO ILS E LNS
#gerar boxplot para todas as instancias de um grupo
groups <- unique(ils_lns_data$GROUP);

group_ <- groups[1]; #debug
for(group_ in groups){
	group_instances <- unique(subset(ils_lns_data, GROUP == group_)$INSTANCE);

	pdf(file = ifelse(TRUE, paste(boxplot_mq_file,group_,".pdf",sep="")));#inicia o pdf
	par(xpd=T);
	dev.set(2);

#instancia_ <- group_instances[1]; #debug
	for (instancia_ in group_instances){
		ils <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$MQ;
		lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$MQ;

		ils <- sapply(sapply(ils, as.character),as.numeric);
		lns <- sapply(sapply(lns, as.character),as.numeric);
		boxplot(ils,lns, names=c("ILS", "LNS"), xlab="MÉTODO", ylab="MQ", main=instancia_);
	}
	dev.off(); #fecha o pdf
	
	#PDF COM O BOXPLOT DO TEMPO
	pdf(file = ifelse(TRUE, paste(boxplot_time_file,group_,".pdf",sep="")));#inicia o pdf
	par(xpd=T);
	dev.set(2);
	for (instancia_ in group_instances){
		ils <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$TIME;
		lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$TIME;

		ils <- sapply(sapply(ils, as.character),as.numeric);
		lns <- sapply(sapply(lns, as.character),as.numeric);
		
		ils <- ils/1000;
		lns <- lns/1000;
	
		boxplot(ils,lns, names=c("ILS", "LNS"), xlab="MÉTODO", ylab="Tempo de processamento (s)", main=instancia_);
	}
	dev.off(); #fecha o pdf
}









#PASSO 2.3 GERAR BOXPLOT PARA AS INSTANCIAS SELECIONADAS (3 POR CATEGORIA)
selected_instances = c(11,32,53,69,79,89,96,102,108,113,117,121);
pdf(file = ifelse(TRUE, paste(boxplot_mq_12_file,".pdf",sep="")), paper="a4", width=8, height=11);#inicia o pdf
dev.set(2);
par(mfrow=c(4,3), pch=22, col="black"); # all plots on one page 
par(mar=c(2, 3, 1.5, 0.5));
par(mgp=c(1.8, 0.6, 0));
for (current_order in selected_instances){
	instancia_ = subset(ils_lns_data, ORDER == current_order & SOLVER == "ILS")$INSTANCE[1];
	ils <- subset(ils_lns_data, ORDER == current_order & SOLVER == "ILS")$MQ;
	lns <- subset(ils_lns_data, ORDER == current_order & SOLVER == "LNS")$MQ;

	ils <- sapply(sapply(ils, as.character),as.numeric);
	lns <- sapply(sapply(lns, as.character),as.numeric);
	boxplot(ils,lns, names=c("ILS", "LNS"), xlab="", ylab="MQ", main=instancia_);
}
dev.off(); #fecha o pdf




	
#PDF COM O BOXPLOT DO TEMPO
pdf(file = ifelse(TRUE, paste(boxplot_time_12_file,".pdf",sep="")), paper="a4", width=8, height=11);#inicia o pdf
dev.set(2);
par(mfrow=c(4,3), pch=22, col="black"); # all plots on one page 
par(mar=c(2, 3, 1.5, 0.5));
par(mgp=c(1.8, 0.6, 0));
for (current_order in selected_instances){
	instancia_ = subset(ils_lns_data, ORDER == current_order & SOLVER == "ILS")$INSTANCE[1];
	ils <- subset(ils_lns_data, ORDER == current_order & SOLVER == "ILS")$TIME;
	lns <- subset(ils_lns_data, ORDER == current_order & SOLVER == "LNS")$TIME;

	ils <- sapply(sapply(ils, as.character),as.numeric);
	lns <- sapply(sapply(lns, as.character),as.numeric);
	
	ils <- ils/1000;
	lns <- lns/1000;

	boxplot(ils,lns, names=c("ILS", "LNS"), xlab="", ylab="Time(s)", main=instancia_);
}
dev.off(); #fecha o pdf	

















#PASSO 3 - GERAR TABELA COMPARATIVA ENTRE LNS E LITERATURA

# Create matrices to hold MQ values
result_lit <- matrix(nrow=length(instances), ncol=6, dimnames=list(instances, c("order","group","avglns", "sdlns", "pv1", "es1")));
# Fill out matrices with data
#instancia_ <- instances[100];#debug

#gerar pvalue e effect size lns vs lit
for (instancia_ in instances){
	group <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$GROUP[1];
	order_ <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$ORDER[1];
	
	lit <- subset(best_literature_data, INSTANCE == instancia_)$MQ; #possui apenas uma linha por instancia
	lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$MQ;
	

	lit <- sapply(sapply(lit, as.character),as.numeric);
	lns <- sapply(sapply(lns, as.character),as.numeric);

	pvalue <- wilcox.test(lit, lns)$p.value;
	es <- vargha.delaney(lit, lns);
	#pv2 <- wilcox.test(lns, um=lit)$p.value;
	#es2 <- length(subset(lns, lns >= lit));
	
	result_lit[instancia_, "order"] <- order_;
	result_lit[instancia_, "group"] <- group;
	result_lit[instancia_, "avglns"] <- mean(lns);
	result_lit[instancia_, "sdlns"] <- sd(lns);
	result_lit[instancia_, "pv1"] <- pvalue;
	result_lit[instancia_, "es1"] <- es/100;
}
round(result_lit,6);









#PASSO 4 - Gerar um grafico comparando o ILSxLNS em tempo de processamento por tamanho de instância
group_name<- c("Small","Medium","Large","Very Large");
groups <- unique(ils_lns_data$GROUP);

#PDF COM O TTT-Plot para cada instancia
	pdf(file = ifelse(TRUE, paste(tttplot_time_lns_ils_file,".pdf",sep="")));#inicia o pdf
	dev.set(2);
	
	layout(matrix(c(1,2,3,4,5,5), ncol=2, byrow=TRUE), heights=c(4, 4, 1));
	par(mar=c(5, 3, 1.5, 0.5));
	par(mgp=c(1.8, 0.6, 0));
for(group_ in groups){
	group_instances <- unique(subset(ils_lns_data, GROUP == group_)$INSTANCE);
	result_time_graph <- matrix(nrow=length(group_instances), ncol=5, dimnames=list(group_instances, c("order","group","modules","time_lns", "time_ils")));
	for (instancia_ in group_instances){
		group <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$GROUP[1];
		order_ <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$ORDER[1];
		
		ils <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$TIME;
		lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$TIME;
		
		modules <- subset(instances_size_data, INSTANCE == instancia_)$MODULES;

		ils <- sapply(sapply(ils, as.character),as.numeric);
		lns <- sapply(sapply(lns, as.character),as.numeric);
		
		ils <- ils/1000;
		lns <- lns/1000;
		
		ils <- mean(ils);
		lns <- mean(lns);
		
		result_time_graph[instancia_, "order"] <- order_;
		result_time_graph[instancia_, "group"] <- group;
		result_time_graph[instancia_, "modules"] <- modules;
		result_time_graph[instancia_, "time_lns"] <- mean(lns);
		result_time_graph[instancia_, "time_ils"] <- mean(ils);
	}
#	result_time_graph;

	xrange <- range(result_time_graph[,"modules"]);
	yrange <- c(0,max(result_time_graph[, "time_lns"],result_time_graph[, "time_ils"]));
	x <- result_time_graph[,"modules"];
	plot(xrange,yrange, type="n", main=paste("Group ",group_name[group_],sep=""), xlab="# of modules", ylab="Time(s)");
	lines(x, result_time_graph[, "time_lns"], col="blue", type="p", pch=3);
	lines(x, result_time_graph[, "time_ils"], col="red", type="p", pch=4);
}

par(mai=c(0,0,0,0))
plot.new()
legend(x="center",c("LNS", "ILS"), pch = c(3,4), col=c("blue","red"), horiz = TRUE);
dev.off();









#PASSO 4.2 - Gerar um grafico comparando o ILSxLNS em MQ por tamanho de instância
group_name<- c("Small","Medium","Large","Very Large");
groups <- unique(ils_lns_data$GROUP);

#PDF COM O TTT-Plot para cada instancia
	pdf(file = ifelse(TRUE, paste(tttplot_mq_lns_ils_file,".pdf",sep="")));#inicia o pdf
	dev.set(2);
	
	layout(matrix(c(1,2,3,4,5,5), ncol=2, byrow=TRUE), heights=c(4, 4, 1));
	par(mar=c(5, 3, 1.5, 0.5));
	par(mgp=c(1.8, 0.6, 0));
for(group_ in groups){
	group_instances <- unique(subset(ils_lns_data, GROUP == group_)$INSTANCE);
	result_time_graph <- matrix(nrow=length(group_instances), ncol=5, dimnames=list(group_instances, c("order","group","modules","mq_lns", "mq_ils")));
	for (instancia_ in group_instances){
		group <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$GROUP[1];
		order_ <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$ORDER[1];
		
		ils <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "ILS")$MQ;
		lns <- subset(ils_lns_data, INSTANCE == instancia_ & SOLVER == "LNS")$MQ;
		
		modules <- subset(instances_size_data, INSTANCE == instancia_)$MODULES;

		ils <- sapply(sapply(ils, as.character),as.numeric);
		lns <- sapply(sapply(lns, as.character),as.numeric);
		
	#	ils <- ils/1000;
	#	lns <- lns/1000;
		
		ils <- mean(ils);
		lns <- mean(lns);
		
		result_time_graph[instancia_, "order"] <- order_;
		result_time_graph[instancia_, "group"] <- group;
		result_time_graph[instancia_, "modules"] <- modules;
		result_time_graph[instancia_, "mq_lns"] <- mean(lns);
		result_time_graph[instancia_, "mq_ils"] <- mean(ils);
	}
#	result_time_graph;

	xrange <- range(result_time_graph[,"modules"]);
	yrange <- c(0,max(result_time_graph[, "mq_lns"],result_time_graph[, "mq_ils"]));
	x <- result_time_graph[,"modules"];
	plot(xrange,yrange, type="n", main=paste("Group ",group_name[group_],sep=""), xlab="# of modules", ylab="MQ");
	lines(x, result_time_graph[, "mq_lns"], col="blue", type="p", pch=3);
	lines(x, result_time_graph[, "mq_ils"], col="red", type="p", pch=4);
}

par(mai=c(0,0,0,0))
plot.new()
legend(x="center",c("LNS", "ILS"), pch = c(3,4), col=c("blue","red"), horiz = TRUE);
dev.off();










selected_group_name=c("SMALL","MEDIUM","LARGE","VERY LARGE");

pdf(file = ifelse(TRUE, paste(boxplot_best_comp_,"k_n",".pdf",sep=""))
, width=8, height=5);#inicia o pdf
par(xpd=T);
dev.set(2);
#comparativo das melhores soluções obtidas
bp_data_perc_P =c(100.00,66.67,30.77,41.67,44.44,35.71,50.00,100.00,66.67,33.33,41.18,58.33,33.33,35.71,25.00,85.71,35.71,46.67,34.78,50.00,39.13,40.91,34.62,38.89,36.84,40.00,22.73,21.05,28.00,20.83,32.14,38.10,38.46,33.33,13.04,30.77,43.33,25.00,33.33,38.24,28.00,28.57,34.21,35.00,31.43,33.33,32.43,34.09,36.73,25.00,34.09,100.00,32.50,31.25,37.04,46.51,25.45,29.41,44.44,46.81,35.90,42.86,35.94,36.76);
bp_data_perc_M =c(41.79,32.00,32.00,44.74,32.10,36.49,40.00,34.88,36.78,31.52,47.54,34.74,52.00,39.80,33.33,40.30,69.84,32.79,34.15,33.86,36.67,44.83,31.00,29.66,95.45,33.75,30.33,40.54,41.60);
bp_data_perc_G =c(39.33,34.72,38.67,35.26,30.81,42.78,38.02,40.11,38.40,33.97,36.69,36.26,41.50,38.37,39.55,32.92,26.57,37.54);
bp_data_perc_MG=c(42.44,30.87,24.30,41.27,39.04,36.81,34.01,37.15,35.96,35.67,47.90,34.83,36.16);
boxplot(bp_data_perc_P,bp_data_perc_M,bp_data_perc_G,bp_data_perc_MG, names=selected_group_name, xlab="", ylab="Ratio between number of clusters and modules * 100%", cex.axis=1, cex.lab=1.5);
dev.off();

bp_data_perc_ALL = c();
bp_data_perc_ALL = append(bp_data_perc_ALL, bp_data_perc_P);
bp_data_perc_ALL = append(bp_data_perc_ALL, bp_data_perc_M);
bp_data_perc_ALL = append(bp_data_perc_ALL, bp_data_perc_G);
bp_data_perc_ALL = append(bp_data_perc_ALL, bp_data_perc_MG);

mean(bp_data_perc_ALL); #39.2308
median(bp_data_perc_ALL); #36.21

pdf(file = ifelse(TRUE, paste(boxplot_best_comp_,"k_n","_no_out.pdf",sep=""))
, width=8, height=5);#inicia o pdf
par(xpd=T);
dev.set(2);
boxplot(bp_data_perc_P,bp_data_perc_M,bp_data_perc_G,bp_data_perc_MG, names=selected_group_name, xlab="", ylab="Ratio between number of clusters and modules * 100%", cex.axis=1, cex.lab=1.5, outline=FALSE);
dev.off();




pdf(file = ifelse(TRUE, paste(boxplot_best_comp_,"n_k",".pdf",sep=""))
, width=8, height=5);#inicia o pdf
par(xpd=T);
dev.set(2);
#comparativo das melhores soluções obtidas
bp_data_frac_P =c(1.00,1.50,3.25,2.40,2.25,2.80,2.00,1.00,1.50,3.00,2.43,1.71,3.00,2.80,4.00,1.17,2.80,2.14,2.88,2.00,2.56,2.44,2.89,2.57,2.71,2.50,4.40,4.75,3.57,4.80,3.11,2.63,2.60,3.00,7.67,3.25,2.31,4.00,3.00,2.62,3.57,3.50,2.92,2.86,3.18,3.00,3.08,2.93,2.72,4.00,2.93,1.00,3.08,3.20,2.70,2.15,3.93,3.40,2.25,2.14,2.79,2.33,2.78,2.72);
bp_data_frac_M =c(2.39,3.13,3.13,2.24,3.12,2.74,2.50,2.87,2.72,3.17,2.10,2.88,1.92,2.51,3.00,2.48,1.43,3.05,2.93,2.95,2.73,2.23,3.23,3.37,1.05,2.96,3.30,2.47,2.40);
bp_data_frac_G =c(2.54,2.88,2.59,2.84,3.25,2.34,2.63,2.49,2.60,2.94,2.73,2.76,2.41,2.61,2.53,3.04,3.76,2.66);
bp_data_frac_MG=c(2.36,3.24,4.12,2.42,2.56,2.72,2.94,2.69,2.78,2.80,2.09,2.87,2.77);
boxplot(bp_data_frac_P,bp_data_frac_M,bp_data_frac_G,bp_data_frac_MG, names=selected_group_name, xlab="", ylab="Modules per cluster", cex.axis=1, cex.lab=1.5);
dev.off();

bp_data_frac_ALL = c();
bp_data_frac_ALL = append(bp_data_frac_ALL, bp_data_frac_P);
bp_data_frac_ALL = append(bp_data_frac_ALL, bp_data_frac_M);
bp_data_frac_ALL = append(bp_data_frac_ALL, bp_data_frac_G);
bp_data_frac_ALL = append(bp_data_frac_ALL, bp_data_frac_MG);

mean(bp_data_frac_ALL); #2.783306
median(bp_data_frac_ALL); #2.765

pdf(file = ifelse(TRUE, paste(boxplot_best_comp_,"n_k","_no_out.pdf",sep=""))
, width=8, height=5);#inicia o pdf
par(xpd=T);
dev.set(2);
boxplot(bp_data_frac_P,bp_data_frac_M,bp_data_frac_G,bp_data_frac_MG, names=selected_group_name, xlab="", ylab="Modules per cluster", cex.axis=1, cex.lab=1.5, outline=FALSE);
dev.off();













#======================================================================
#============================PARA O ARTIGO=============================
#======================================================================







#BOXPLOT COM GAP MEDIO POR CATEGORIA
selected_group=c(1,2,3,4);
pdf(file = ifelse(TRUE, paste(boxplot_mq_rel_file,".pdf",sep=""))#, paper="a4"
, width=8, height=5);#inicia o pdf
dev.set(2);
#par(mfrow=c(1,1), pch=22, col="black"); # all plots on one page 
#par(mar=c(2, 3, 1.5, 0.5));
#par(mgp=c(1.8, 0.6, 0));
box_p_data_1 <- c();
box_p_data_2 <- c();
box_p_data_3 <- c();
box_p_data_4 <- c();

for (current_group in selected_group){
	
	group_instances <- unique(subset(ils_lns_data, GROUP == current_group)$INSTANCE);
	for(instance in group_instances){
		ils <- subset(ils_lns_data, INSTANCE == instance & SOLVER == "ILS")$MQ;
		lns <- subset(ils_lns_data, INSTANCE == instance & SOLVER == "LNS")$MQ;

		ils <- sapply(sapply(ils, as.character),as.numeric);
		lns <- sapply(sapply(lns, as.character),as.numeric);
		
		mean_ils <- mean(ils);
		mean_lns <- mean(lns);
		
		dif <- (mean_lns - mean_ils)/mean_ils;
		dif <- dif*100;
		if(current_group ==1){box_p_data_1 <- append(box_p_data_1,dif);}
		if(current_group ==2){box_p_data_2 <- append(box_p_data_2,dif);}
		if(current_group ==3){box_p_data_3 <- append(box_p_data_3,dif);}
		if(current_group ==4){box_p_data_4 <- append(box_p_data_4,dif);}
	}
}
boxplot(box_p_data_1,box_p_data_2,box_p_data_3,box_p_data_4, names=selected_group_name, xlab="", ylab="MQ Difference", main="", cex.axis=1, cex.lab=1.5);
dev.off(); #fecha o pdf




pdf(file = ifelse(TRUE, paste(boxplot_mq_rel_file_no_out,".pdf",sep=""))#, paper="a4"
, width=8, height=5);#inicia o pdf
dev.set(2);
#par(mfrow=c(1,1), pch=22, col="black"); # all plots on one page 
#par(mar=c(2, 3, 1.5, 0.5));
#par(mgp=c(1.8, 0.6, 0));
boxplot(box_p_data_1,box_p_data_2,box_p_data_3,box_p_data_4, names=selected_group_name, xlab="", ylab="MQ Difference", main="", cex.axis=1, cex.lab=1.5, outline=FALSE);
dev.off(); #fecha o pdf




	
#PDF COM O BOXPLOT DO TEMPO
selected_group=c(1,2,3,4);
selected_group_name=c("SMALL","MEDIUM","LARGE","VERY LARGE");
pdf(file = ifelse(TRUE, paste(boxplot_time_rel_file,".pdf",sep=""))#, paper="a4"
, width=8, height=5);#inicia o pdf
dev.set(2);
#par(mfrow=c(1,1), pch=22, col="black"); # all plots on one page 
#par(mar=c(2, 3, 1.5, 0.5));
#par(mgp=c(1.8, 0.6, 0));
box_p_data_1 <- c();
box_p_data_2 <- c();
box_p_data_3 <- c();
box_p_data_4 <- c();

for (current_group in selected_group){
	
	group_instances <- unique(subset(ils_lns_data, GROUP == current_group)$INSTANCE);
	for(instance in group_instances){
		ils <- subset(ils_lns_data, INSTANCE == instance & SOLVER == "ILS")$TIME;
		lns <- subset(ils_lns_data, INSTANCE == instance & SOLVER == "LNS")$TIME;

		ils <- sapply(sapply(ils, as.character),as.numeric);
		lns <- sapply(sapply(lns, as.character),as.numeric);
	
		ils <- ils/1000;
		lns <- lns/1000;
		
		mean_ils <- mean(ils);
		mean_lns <- mean(lns);
		
		dif <- (mean_lns - mean_ils)/mean_ils;
		dif <- dif*100;
		if(current_group ==1){box_p_data_1 <- append(box_p_data_1,dif);}
		if(current_group ==2){box_p_data_2 <- append(box_p_data_2,dif);}
		if(current_group ==3){box_p_data_3 <- append(box_p_data_3,dif);}
		if(current_group ==4){box_p_data_4 <- append(box_p_data_4,dif);}
	}
}
boxplot(box_p_data_1,box_p_data_2,box_p_data_3,box_p_data_4, names=selected_group_name, xlab="", ylab="Time Difference", main="", cex.axis=1, cex.lab=1.5);
dev.off(); #fecha o pdf	







pdf(file = ifelse(TRUE, paste(boxplot_time_rel_file_no_out,".pdf",sep=""))#, paper="a4" 
, width=8, height=5);#inicia o pdf
dev.set(2);
par(mfrow=c(1,1), pch=22, col="black"); # all plots on one page 
par(mar=c(2, 3, 1.5, 0.5));
par(mgp=c(1.8, 0.6, 0));
boxplot(box_p_data_1,box_p_data_2,box_p_data_3,box_p_data_4, names=selected_group_name, xlab="", ylab="Time Difference", main="", cex.axis=1, cex.lab=1.5,outline=FALSE);
dev.off(); #fecha o pdf	

