######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### CONFIGURA��O BASE ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

#caminho para os arquivos
#base_path = "E:/source_codes/SMC/experiment_output/";
base_path = "D:/Marlon/PPGI/SMC/experiment_output/";
experiment_folder = "1412820860509";
output_folder = "r_output";

#diretorios de entrada e saida
#input_path = paste(base_path,experiment_folder,"/",sep="");
#output_path = paste(base_path,output_folder,"/",sep="");

#install.packages('plyr');
require(plyr);

#install.packages('R2HTML');
library(R2HTML) #saida para html

#Valores base para os graficos
graph_colors <- c("blue","black","brown","red","pink","purple","gray","gold4","yellow","magenta","navy","orange","orangered","tan1","yellowgreen","rosybrown","red4","palegreen4");
grapth_pch <- c(15,16,17,18,19,23);
default_inset <- -0.2;
small_inset <- -0.25;



######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### DECLARA��O DAS FUNC��ES ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

#function_nested_folders_names - VERIFICA AS PASTAS DENTRO DE input_folder E RETORNA O NOME DE CADA UMA
function_nested_folders_names = function(input_path){
	#print(paste("LISTING FOLDERS WITHIN:",input_path));
	flush.console();
	instances_names = list.dirs(input_path, full.names=FALSE);
	return (instances_names[2:length(instances_names)]);
}

#function_find_files - Recupera uma lista com os nomes dos arquivos que estao em uma pasta e que iniciam com prefix
function_find_files = function(input_folder, prefix){
	#print(paste("FILTRANDO ARQUIVOS EM:",input_folder,"COM PREFIXO",prefix));
	file_pattern = paste(prefix,".*.dat",sep="");
	files_names = list.files(path=input_folder, pattern = file_pattern, all.files = FALSE, full.names = FALSE, recursive = FALSE, ignore.case = FALSE, include.dirs = FALSE);
	return(files_names);
}

#function_read_data_file - Efetua a leitura do aqurivo de dados bruto ou dos dados com melhores itera��o
function_read_data_file = function(input_file, rows=NULL){
	if(is.null(rows)){
		file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
			,colClasses=c("character","integer","character","character","integer","double","double","integer","integer","integer","integer","integer","integer","double","double")
			)
		;
	}else{
		file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
			,colClasses=c("character","integer","character","character","integer","double","double","integer","integer","integer","integer","integer","integer","double","double")
			, nrows = rows
			)
		;
	}
	colnames(file_data) <- c("INSTANCE","SIZE","ALGO","CONFIG","ITERATION","CURRENT_MQ","BEST_MQ","ITERATION_BEST_MQ","TIME","TOTAL_CLUSTERS","BIGGEST_CLUSTER","SMALLEST_CLUSTER","ISLATED_CLUSTERS","LOWER_CLUSTER_MF","HIGHER_CLUSTER_MF");
	return (file_data);
}

#function_read_resumed_data_file - Efetua a leitura do aqurivo de dados que cont�m os dados resumidos (last_iterations)
function_read_resumed_data_file = function(input_file){
	#print(input_file);
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","character","character","double","double","double","double","integer","integer","double","double"))
	;
	colnames(file_data) <- c("INSTANCE","ALGO","CONFIG","BEST_MQ_max","BEST_MQ_min","BEST_MQ_mean","BEST_MQ_sd","TIME_max","TIME_min","TIME_mean","TIME_sd");
	return (file_data);
}

#function_read_resumed_configs_ordered_file - Efetua a leitura do aqurivo de dados que cont�m os dados das configs ordenadas da melhor para a pior
function_read_resumed_configs_ordered_file = function(input_file){
	#print(input_file);
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","integer","integer","integer","integer"))
	;				
	colnames(file_data) <- c("CONFIG","TOTAL_WIN_MQ","SUM_POSITION_MQ","TOTAL_WIN_TIME","SUM_POSITION_TIME");
	return (file_data);
}

#function_read_instance_best_file - Efetua a leitura do arquivo de instancia (com dados gerais)
function_read_instance_best_file = function(input_file){
	#print(input_file);
	flush.console();
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","character", "character", "integer", "integer", "character", "double", "double", "double", "double"))
	;				
	colnames(file_data) <- c("INSTANCE","I_HAVE", "WEIGHTED", "MODULES", "DEPENDENCIES", "IS_OPTIMUM", "BEST_KNOWN_MQ", "MQ_STD", "TIME", "TIME_STD");
	return (file_data);
}

#function_read_best_data_file - Efetua a leitura do arquivo com os dados comparativos entre os meus resultados e os melhores da literatura
function_read_best_data_file = function(input_file){
	#print(input_file);
	flush.console();
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","character","character","double","double","double","double","integer","integer","double","double","double","double"))
	;				
	
	colnames(file_data) <- c("INSTANCE","ALGO","CONFIG","BEST_MQ_max","BEST_MQ_min","BEST_MQ_mean","BEST_MQ_sd","TIME_max","TIME_min","TIME_mean","TIME_sd","BEST_KNOWN_MQ","DISTANCE_FROM_BEST");
	return (file_data);
}

#function_read_temperature_file - Efetua a leitura do arquivo com os dados comparativos entre os meus resultados e os melhores da literatura
function_read_temperature_file = function(input_file){
	#print(input_file);
	flush.console();
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","double","double","integer","double"))
	;				
	#CAMQ;0.99951;0.1;840;0.24613889167655706
	colnames(file_data) <- c("ALGO","CR","ITR","ITERATION","TEMPERATURE");
	return (file_data);
}

#function_find_mq_changes - Recupera uma lista que contem cada linha onde o MQ sofreu altera��o
function_find_mq_changes = function(file_data) {
	last_read_line="";
	saved_lines = data.frame();
	max_iteration=max(file_data$ITERATION);
	for(n in 1:nrow(file_data)){
		current_line = file_data[n,];
		if(current_line$ITERATION==0 || current_line$ITERATION==max_iteration || last_read_line$BEST_MQ != current_line$BEST_MQ){
			saved_lines = rbind(saved_lines,current_line);
		}
		last_read_line = current_line;
	}
	return (saved_lines);
}

function_find_last_lines = function(file_data){
	max_iteration=max(file_data$ITERATION);
	last_iterations = file_data[ which(file_data$ITERATION==max_iteration), ];
	return(last_iterations);
}

#process_instance_data - PROCESSA OS DADOS DE TODOS OS ARQUIVOS DA INSTANCIA
process_instance_data = function (input_path, instance_name, method){
	instance_folder = paste(input_path,instance_name,sep="");
	method_files = function_find_files(instance_folder,method);

	instance_all_mq_changes = data.frame();
	instance_all_last_iteration = data.frame();
	for(method_file in method_files){
		print(paste("Processando arquivo: ",method_file));
		file_with_path=paste(instance_folder,method_file,sep="/");
		flush.console();
		file_data = function_read_data_file(file_with_path);
		mq_changes = function_find_mq_changes(file_data);
		last_lines = function_find_last_lines(file_data);
		
		instance_all_mq_changes = rbind(instance_all_mq_changes,mq_changes);
		instance_all_last_iteration = rbind(instance_all_last_iteration, last_lines);
	}
	return (list(mq_changes=instance_all_mq_changes, last_iterations=instance_all_last_iteration))
}

process_config_instance_data = function (input_path, instance_name, method, config){
	instance_folder = paste(input_path,instance_name,sep="");
	method_files = function_find_files(instance_folder,method);
	for(method_file in method_files){
		file_with_path=paste(instance_folder,method_file,sep="/");
		file_data = function_read_data_file(file_with_path,1);#basta ler a primeira linha para saber se � o arquivo
		if(file_data$CONFIG[1] == config){#processar o arquivo
			print(paste(instance_name,": PROCESSANDO APENAS O ARQUIVO:",file_with_path));
			flush.console();
			file_data = function_read_data_file(file_with_path);
			return (function_find_mq_changes(file_data));
		}
	}
}

#function_build_abstract_data - gera os dados estatisticos da instancia
function_build_abstract_data = function(instance_data){
	processed_data = ddply(instance_data, c('INSTANCE','ALGO','CONFIG'), function(x) c(
		BEST_MQ_max=max(x$BEST_MQ), BEST_MQ_min=min(x$BEST_MQ), BEST_MQ_mean=mean(x$BEST_MQ), BEST_MQ_sd=sd(x$BEST_MQ)
		, TIME_max=max(x$TIME), TIME_min=min(x$TIME), TIME_mean=mean(x$TIME), TIME_sd=sd(x$TIME)
		));
	return (processed_data);
}

create_rank_on_data = function(instance_data){
	mq_ranked_data = transform(instance_data, MQ_RANK = ave(BEST_MQ_mean, FUN = function(x) rank(-x, ties.method = "min")));
	ranked_data = transform(mq_ranked_data, TIME_RANK = ave(TIME_mean, FUN = function(x) rank(x, ties.method = "min")));
	return (ranked_data);
}

rank_algorithm_parameters = function(resumed_data){
	instance_names = unique(resumed_data$INSTANCE);
	ranked_data = data.frame();
	for(instance_name in instance_names){
		instance_data = subset(resumed_data, resumed_data$INSTANCE==instance_name);
		ranked_data = rbind(ranked_data,create_rank_on_data(instance_data));
	}
	config_group = ddply(ranked_data, c('CONFIG'), function(x) c(
		TOTAL_WIN_MQ=sum(x$MQ_RANK==1), SUM_POSITION_MQ=sum(x$MQ_RANK), TOTAL_WIN_TIME=sum(x$TIME_RANK==1), SUM_POSITION_TIME=sum(x$TIME_RANK)
		)
	);
	order_control = order(-config_group$TOTAL_WIN_MQ, config_group$SUM_POSITION_MQ, -config_group$TOTAL_WIN_TIME, config_group$SUM_POSITION_TIME);
	
	return(config_group[order_control,]);
}

create_exectution_times_list = function(mq_changes_data){
	retun_data = list();
	current_frame = data.frame();
	for(i in 1:nrow(mq_changes_data)){
		current_row = mq_changes_data[i,];
		if(current_row$ITERATION==0){
			if(i != 1){
				retun_data = append(retun_data,list(current_frame));
			}
			current_frame = data.frame();
		}
		current_frame = rbind(current_frame, current_row);
	}
	retun_data = append(retun_data,list(current_frame));#adiciona o ultimo
	return (retun_data);
}

merge_best_configs_data = function(resumed_data, all_config_data, total_configs){
	return_data = data.frame();
	instance_names = unique(resumed_data$INSTANCE);
	for(instance_name in instance_names){
		for (i in 1:total_configs){
			current_config = all_config_data[i,]$CONFIG;
			current_instance_data =  resumed_data[which(resumed_data$INSTANCE==instance_name & resumed_data$CONFIG==current_config), ];
			return_data = rbind(return_data,current_instance_data);
		}
	}
	return (return_data);
}

merge_best_results = function(literature_best_results, my_best_results){
	best_data = data.frame();
	for(i in 1: nrow(my_best_results)){
		line_data = my_best_results[i, ];
		instance_best_data = literature_best_results[which(literature_best_results$INSTANCE==line_data$INSTANCE), ];
		best_data = rbind(best_data, instance_best_data);
	}
	my_best_results$BEST_KNOWN_MQ = best_data$BEST_KNOWN_MQ;#(best_data$BEST_KNOWN_MQ / my_best_results$BEST_MQ_mean)
	my_best_results$DISTANCE_FROM_BEST = 1-(my_best_results$BEST_MQ_mean / best_data$BEST_KNOWN_MQ );
	
	print(my_best_results);
}


######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### FUN�OES GERADORAS DE GR�FICOS ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

create_comparative_mq_instance_graph = function (graph_data){
	y_max = max(graph_data$BEST_MQ_max);
	y_min = min(graph_data$BEST_MQ_min);
	x_min = 1;
	x_max = length(graph_data[,1]);
	plot(c(x_min,x_max),c(y_min,y_max), type="n", main=paste(graph_data[1,1],"-","MQ x Algoritmo"));
	for(i in 1:length(graph_data[,1])){
		current_data = graph_data[i,];
		points(i, current_data$BEST_MQ_max, type="p", pch=2, col=i);
		points(i, current_data$BEST_MQ_mean, type="p", pch=1, col=i);
		points(i, current_data$BEST_MQ_min, type="p", pch=6, col=i);
	}
}

create_comparative_time_instance_graph = function (graph_data){
	y_max = max(graph_data$TIME_max);
	y_min = min(graph_data$TIME_min);
	x_min = 1;
	x_max = length(graph_data[,1]);
	plot(c(x_min,x_max),c(y_min,y_max), type="n", main=paste(graph_data[1,1],"-","TIME x Algoritmo"));
	for(i in 1:length(graph_data[,1])){
		current_data = graph_data[i,];
		points(i, current_data$TIME_max, type="p", pch=2, col=i);
		points(i, current_data$TIME_mean, type="p", pch=1, col=i);
		points(i, current_data$TIME_min, type="p", pch=6, col=i);
	}
}


generate_iteration_graph = function(mq_changes_data, lines_limit=NULL){
	if(is.null(lines_limit) || lines_limit>length(mq_changes_data)){
		total_lines=length(mq_changes_data);
	}else{
		total_lines = lines_limit;
	}
	
	rand_execution = sample(1:length(mq_changes_data), total_lines, replace=F);
	
	min_x = min(mq_changes_data[[rand_execution[1]]]$ITERATION);
	max_x = max(mq_changes_data[[rand_execution[1]]]$ITERATION);
	min_y = min(mq_changes_data[[rand_execution[1]]]$BEST_MQ);
	max_y = max(mq_changes_data[[rand_execution[1]]]$BEST_MQ);
	
	for (i in 2:total_lines){
		min_x = min(min_x,mq_changes_data[[rand_execution[i]]]$ITERATION);
		max_x = max(max_x,mq_changes_data[[rand_execution[i]]]$ITERATION);
		min_y = min(min_y,mq_changes_data[[rand_execution[i]]]$BEST_MQ);
		max_y = max(max_y,mq_changes_data[[rand_execution[i]]]$BEST_MQ);
	}
	plot(c(min_x,max_x),c(min_y,max_y), type="n", main=paste("Evolu��o do MQ inst�ncia:",mq_changes_data[[1]]$INSTANCE[1],"config:",mq_changes_data[[1]]$CONFIG[1]));
	
	for(i in 1:total_lines){
		lines(mq_changes_data[[rand_execution[i]]]$ITERATION, mq_changes_data[[rand_execution[i]]]$BEST_MQ, cex = 1
		, type="o"
		, pch=16, col=graph_colors[i%%length(graph_colors)+1]
		);
	}
}


generate_compare_with_best_graph = function(instance_name, graph_data){
	min_x = 1;
	max_x = max(nrow(graph_data));
	min_y = min(graph_data$BEST_MQ_mean, graph_data$BEST_KNOWN_MQ );
	max_y = max(graph_data$BEST_MQ_mean, graph_data$BEST_KNOWN_MQ);
	plot(c(min_x,max_x),c(min_y,max_y), type="n", main=paste("Compara��o de MQ para a inst�ncia:",instance_name));
	
	for(i in 1:nrow(graph_data)){
		lines(i, graph_data[i, ]$BEST_KNOWN_MQ, cex = 1
		, type="b"
		, pch=16, col=graph_colors[1]
		);
		
		lines(i, graph_data[i, ]$BEST_MQ_mean, cex = 1
		, type="o"
		, pch=16, col=graph_colors[2]
		);
	}
	
}


function_sa_temperature_graph = function(instance_name, graph_data){
	#configs
	algo = unique(graph_data$ALGO);
	cr = unique(graph_data$CR);
	itr = unique(graph_data$ITR);
	
	
	
	for(current_cr in cr){
		i=1;
		size=2;
		legend_text = c();
		used_colors = c();
		used_symbols = c();
		
		cr_data = graph_data[ which(graph_data$CR==current_cr), ];
		
		min_x = 0;	
		max_x = max(cr_data$ITERATION);
		min_y = min(cr_data$TEMPERATURE);
		max_y = max(cr_data$TEMPERATURE);
		plot(
			c(min_x,max_x),c(min_y,max_y), type="n", main=paste("Decaimento da temperatura para :",instance_name)
			, xlab="ITERA��O"
			, ylab="TEMPERATURA"
		);
		
		for(current_algo in algo){
			algo_data = cr_data[ which(cr_data$ALGO==current_algo), ];
			for(current_itr in itr){
				legend_text = append(legend_text, paste(current_algo,current_cr,current_itr));
				used_colors = append(used_colors, graph_colors[i]);
				used_symbols = append(used_symbols,"l");
				itr_data = algo_data[ which(algo_data$ITR==current_itr), ];
				lines(itr_data$ITERATION, itr_data$TEMPERATURE, itr_data[i, ]$TEMPERATURE
					#, cex = 1
					, type="l"
					#, pch=16
					, col=graph_colors[i]
				);
				i=i+1;
			}
		}
		
		legend(
			"topright" #position
			, legend_text #text
			, lty= 1 # gives the legend appropriate symbols (lines)
			, cex=0.5
			, col = graph_colors #colors
		);
	}
}









######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### PASSOS PRINCIPAIS ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

#function_step_0 - CRIA OS DIRETORIOS DE SAIDA DE DADOS
function_step_0 = function (base_path, experiment_folder, output_folder){
	dir.create(file.path(base_path, output_folder), showWarnings = FALSE);#cria o diretorio de saida padrao
	output_path = paste(base_path,output_folder,"/",sep="");

	dir.create(file.path(output_path, experiment_folder), showWarnings = FALSE);#cria o diretorio de saida da execucao
	output_path = paste(output_path,experiment_folder,"/",sep="");
}

#function_step_1 - Gera os dados resumidos de cada instancia e configura��o
function_step_1 = function(base_path, experiment_folder, output_folder){
	instance_name_list = function_nested_folders_names(paste(base_path,experiment_folder,sep=""));
	instance_data_LNS_abstract = data.frame();
	for(instance_name in instance_name_list){
		input_path = paste(base_path,experiment_folder,"/",sep="");#ler da pasta dos arquivos criados
		instance_processed_data_LNS = process_instance_data(input_path,instance_name,"LNS");
		mq_changes = instance_processed_data_LNS$mq_changes;
		last_iterations = instance_processed_data_LNS$last_iterations;
		instance_data_LNS_abstract = rbind(instance_data_LNS_abstract,function_build_abstract_data(last_iterations));
	}
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#gerar um arquivo apenas
	print("Gerando arquivos CSV e HTML com dados das ultimas itera��es: ");
	flush.console();
	HTML(instance_data_LNS_abstract, file = paste(output_path,"last_iterations",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
	write.table(instance_data_LNS_abstract, file = paste(output_path,"last_iterations",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
}

function_step_2 = function(base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	input_file = paste(output_path,"last_iterations",".csv",sep="");
	resumed_data = function_read_resumed_data_file(input_file);
	group_data = rank_algorithm_parameters(resumed_data);
	print("Gerando arquivos CSV e HTML com dados das melhores configs: ");
	flush.console();
	HTML(group_data, file = paste(output_path,"configs_ordered",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
	write.table(group_data, file = paste(output_path,"configs_ordered",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
}

function_step_3 = function(base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	input_file = paste(output_path,"last_iterations",".csv",sep="");
	resumed_data = function_read_resumed_data_file(input_file);
	input_file_configs = paste(output_path,"configs_ordered",".csv",sep="");
	all_config_data = function_read_resumed_configs_ordered_file(input_file_configs);
	instance_name_list = function_nested_folders_names(paste(base_path,experiment_folder,sep=""));
	resumed_data = merge_best_configs_data(resumed_data, all_config_data, 10);
	
	HTML(resumed_data, file = paste(output_path,"best_configs_results",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
	write.table(resumed_data, file = paste(output_path,"best_configs_results",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
}

function_step_4 = function(base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	file_literature_best_results = paste(base_path,"instancias",".csv",sep="");
	literature_best_results = function_read_instance_best_file(file_literature_best_results);
	file_my_best_results = paste(output_path,"best_configs_results",".csv",sep="");
	my_best_results = function_read_resumed_data_file(file_my_best_results);
	merged_data = merge_best_results(literature_best_results, my_best_results);
	
	HTML(merged_data, file = paste(output_path,"my_best_with_literature_best",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
	write.table(merged_data, file = paste(output_path,"my_best_with_literature_best",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
}

#function_graph_1 - GERA OS GR�FICOS DE COMPARATIVO DE ALGORITMO POR MQ
function_graph_1 = function (base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	input_file = paste(output_path,"best_configs_results",".csv",sep="");
	resumed_data = function_read_resumed_data_file(input_file);
	instance_names = unique(resumed_data$INSTANCE);
	cols=2;
	rows=length(instance_names)/cols;
	graph_base_size=4;
	pdf(file = ifelse(TRUE, paste(output_path,"comparative_mq_instance_graph",".pdf",sep="")),2*graph_base_size*cols,graph_base_size*rows);
	dev.set(2);
	
	par(mfrow=c(rows, cols));#criar espa�os para os graficos
	for(instance_name in instance_names){
		instance_data = subset(resumed_data,INSTANCE==instance_name);
		create_comparative_mq_instance_graph(instance_data);
	}
	dev.off();
}

#function_graph_2 - GERA OS GR�FICOS DE COMPARATIVO DE ALGORITMO POR TEMPO
function_graph_2 = function (base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	input_file = paste(output_path,"best_configs_results",".csv",sep="");
	resumed_data = function_read_resumed_data_file(input_file);
	instance_names = unique(resumed_data$INSTANCE);
	cols=2;
	rows=length(instance_names)/cols;
	graph_base_size=4;
	pdf(file = ifelse(TRUE, paste(output_path,"comparative_time_instance_graph",".pdf",sep="")),2*graph_base_size*cols,graph_base_size*rows);
	dev.set(2);
	
	par(mfrow=c(rows, cols));#criar espa�os para os graficos
	for(instance_name in instance_names){
		instance_data = subset(resumed_data,INSTANCE==instance_name);
		create_comparative_time_instance_graph(instance_data);
	}
	dev.off();
}

#function_graph_3 - GERA OS GR�FICOS DE ITERA��O
function_graph_3 = function (base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	input_file = paste(output_path,"configs_ordered",".csv",sep="");
	all_config_data = function_read_resumed_configs_ordered_file(input_file);
	instance_names = function_nested_folders_names(paste(base_path,experiment_folder,sep=""));
	total_config_graph=10;
	
	cols=total_config_graph;
	rows=length(instance_names);
	graph_base_size=4;
	pdf(file = ifelse(TRUE, paste(output_path,"iteration_growth_graph",".pdf",sep="")),2*graph_base_size*cols,graph_base_size*rows);
	dev.set(2);
	
	par(mfrow=c(rows, cols));#criar espa�os para os graficos
	input_path = paste(base_path,experiment_folder,"/",sep="");#ler da pasta dos arquivos criados
	
	

	for(instance_name in instance_names){
		for(config_n in 1:total_config_graph){
			config_to_use = all_config_data$CONFIG[config_n];#melhor config
			
			mq_changes = process_config_instance_data(input_path,instance_name,"LNS",config_to_use);
			mq_changes_times = create_exectution_times_list(mq_changes);
			generate_iteration_graph(mq_changes_times,5);
		}
	}
	
	dev.off();
}

#function_graph_4 - comparativo entre meus resultados e gmelhores da literatura
function_graph_4 = function (base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	input_file = paste(output_path,"my_best_with_literature_best",".csv",sep="");
	all_config_data = function_read_best_data_file(input_file);
	instance_names = unique(all_config_data$INSTANCE);
	
	cols=1;
	rows=length(instance_names);
	graph_base_size=4;
	pdf(file = ifelse(TRUE, paste(output_path,"compare_with_best_graph",".pdf",sep="")),2*graph_base_size*cols,graph_base_size*rows);
	dev.set(2);
	
	par(mfrow=c(rows, cols));#criar espa�os para os graficos
	input_path = paste(base_path,experiment_folder,"/",sep="");#ler da pasta dos arquivos criados
	
	

	for(instance_name in instance_names){
		graph_data = all_config_data[ which(all_config_data$INSTANCE==instance_name), ];
		generate_compare_with_best_graph(instance_name, graph_data);
	}
	
	dev.off();
}





#function_graph_sa - Gera o gr�fico de temperatura para o SA
function_graph_sa = function(base_path, experiment_folder, output_folder){
	output_path = paste(base_path,output_folder,"/",experiment_folder,"/",sep="");#ler um unico arquivo
	instance_names = function_nested_folders_names(paste(base_path,experiment_folder,sep=""));
	instance_data_LNS_abstract = data.frame();
	
	cols=3;
	rows=length(instance_names);
	graph_base_size=4;
	pdf(file = ifelse(TRUE, paste(output_path,"sa_temperature",".pdf",sep="")),2*graph_base_size*cols,graph_base_size*rows);
	dev.set(2);
	
	par(mfrow=c(rows, cols));#criar espa�os para os graficos
	for(instance_name in instance_names){
		input_path = paste(base_path,experiment_folder,"/",sep="");#ler da pasta dos arquivos criados
		input_path = paste(input_path,instance_name,"/",sep="");
		method_files = function_find_files(input_path,"SA");
		all_data = function_read_temperature_file(paste(input_path,method_files[1],sep=""));#Apenas um arquivo por instancia
		
		function_sa_temperature_graph(instance_name, all_data);
	}
	dev.off();
}

######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### EXECU��O PRINCIPAL ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 
#function_step_0(base_path, experiment_folder, output_folder);#cria os diretorios de saida
#function_step_1(base_path, experiment_folder, output_folder);#Le os arquivos de saida do algoritmo e gera o arquivo com os dados condensados
#function_step_2(base_path, experiment_folder, output_folder);#Contar a vit�ria de cada algoritmo e tamb�m somar a posi��o de cada um deles
#function_step_3(base_path, experiment_folder, output_folder);#Colocar o valor obtido para os melhores algoritmos executados
#function_step_4(base_path, experiment_folder, output_folder);#Compara os melhores obtidos com os melhores da literatura


#function_graph_1(base_path, experiment_folder, output_folder);#gera os gr�ficos de comparativo de algoritmo por MQ
#function_graph_2(base_path, experiment_folder, output_folder);#gera os gr�ficos de comparativo de algoritmo por TEMPO
#function_graph_3(base_path, experiment_folder, output_folder);#Graficos de crescimento a cada itera��o
#function_graph_4(base_path, experiment_folder, output_folder);#Gr�ficos comparativos entre o mq obtido pela config e o melhor da literatura
function_graph_sa(base_path, experiment_folder, output_folder);
	