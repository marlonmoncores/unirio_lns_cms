@rem for /f "delims=" %%f in ('dir /b /a-d-h-s') do echo %%f
@echo off
FOR %%i IN (experiment_output/*.graph) DO (
	ECHO %%i
	dot -Tpng -O "./experiment_output/%%i"
)
