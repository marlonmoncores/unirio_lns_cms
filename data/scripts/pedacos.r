










process_experiment_data = function (input_folder,output_file){
	#ler arquivos do diretório
	files_names = list.files(path=input_folder, pattern = "*.txt", all.files = FALSE, full.names = FALSE, recursive = FALSE, ignore.case = FALSE, include.dirs = FALSE);

	#para cada arquivo de instancia
	proc_files_names = c();
	proc_names = c();
	proc_mq_mean = c();
	proc_mq_std = c();
	proc_time_mean = c();
	proc_time_std = c();
	for(file_name in files_names) {
		current_file_data <- read.table(paste(input_folder,file_name,sep=""), header=TRUE, sep=";", dec=".", fileEncoding="utf8", colClasses=c("character","double","double"));
		for(current_instance_name in instances_names){
			current_instance_data = subset(current_file_data,INSTANCE==current_instance_name);
			
			valid_times = current_instance_data$TEMPO[-1];
			valid_mqs = current_instance_data$MQ[-1];
			proc_files_names = append(proc_files_names,file_name);
			proc_names = append(proc_names,current_instance_name);
			proc_mq_mean = append(proc_mq_mean,mean(valid_mqs));
			proc_mq_std = append(proc_mq_std,sd(valid_mqs));
			proc_time_mean = append(proc_time_mean,mean(valid_times));
			proc_time_std = append(proc_time_std,sd(valid_times));
		}
	}
	data_frame = data.frame (proc_files_names,proc_names,proc_mq_mean,proc_mq_std,proc_time_mean,proc_time_std);
	names(data_frame) <- c("ALGORITMO", "INSTÂNCIA","MQ MEAN","MQ STD", "TIME MEAN", "TIME STD");

	#Gerar HTML com o data_frame
	HTML(data_frame, file = , Border = 0, innerBorder=1, append=FALSE, digits=5);
}









process_tabu_experiment_data = function (input_folder,output_file){
	#ler arquivos do diretório
	files_names = list.files(path=input_folder, pattern = "*.dat", all.files = FALSE, full.names = FALSE, recursive = FALSE, ignore.case = FALSE, include.dirs = FALSE);

	#para cada arquivo de instancia
	pre_config_data = data.frame();
	proc_algorithm = c();
	proc_parameter = c();
	proc_names = c();
	proc_mq_mean = c();
	proc_mq_std = c();
	proc_time_mean = c();
	proc_time_std = c();
	for(file_name in files_names) {
		#current_file_data <- read.table(paste(input_folder,file_name,sep=""), header=FALSE, sep=";", dec=".", fileEncoding="utf8", colClasses=c("character","integer","character","character","integer","double","double","integer","integer","integer","integer","integer","integer","double","double"));
		current_file_data <- read.table(paste(input_folder,file_name,sep=""), header=FALSE, sep=";", dec=".", fileEncoding="utf8", colClasses=c("character","integer","character","character","integer","double","double","integer","double","integer","integer","integer","integer","double","double"));
		colnames(current_file_data) <- c("INSTANCIA","TAMANHO","ALGORITMO","PARAMETROS","ITERACAO","CURRENT_MQ","MELHOR_MQ","ITERACAO_MELHOR_MQ","TEMPO","TOTAL_CLUSTERES","MAIOR_CLUSTER","MENOR_CLUSTER","CLUSTERES_ISOLADOS","MENOR_MF","MAIOR_MF");
		current_instance_name = current_file_data$INSTANCIA[1];
		
		algorithms_executed = unique(current_file_data$ALGORITMO);
		parameters_executed = unique(current_file_data$PARAMETROS);
		for(current_algorithm in algorithms_executed){
			for(current_parameter in parameters_executed){
				current_configuration_data = subset(current_file_data,ALGORITMO == current_algorithm & PARAMETROS == current_parameter);
				if(nrow(current_configuration_data) > 0){#apenas se houver dado
					pre_config_data = rbind(pre_config_data,tail(current_configuration_data,1));
				}
			}
		}
	}
	
	for(current_instance_name in instances_names){
		current_instance_data = subset(pre_config_data,INSTANCIA==current_instance_name);
		algorithms_executed = unique(current_instance_data$ALGORITMO);
		parameters_executed = unique(current_instance_data$PARAMETROS);
		for(current_algorithm in algorithms_executed){
			for(current_parameter in parameters_executed){
				current_configuration_data = subset(current_instance_data,ALGORITMO == current_algorithm & PARAMETROS == current_parameter);
				if(nrow(current_configuration_data) > 0){#apenas se houver dado
					last_valid_data = current_configuration_data;
					valid_times = current_configuration_data$TEMPO[-1];
					valid_mqs = current_configuration_data$MELHOR_MQ[-1];
					
					proc_algorithm = append(proc_algorithm,current_algorithm);
					proc_parameter = append(proc_parameter,current_parameter);
					proc_names = append(proc_names,current_instance_name);
					proc_mq_mean = append(proc_mq_mean,mean(valid_mqs));
					proc_mq_std = append(proc_mq_std,sd(valid_mqs));
					proc_time_mean = append(proc_time_mean,mean(valid_times));
					proc_time_std = append(proc_time_std,sd(valid_times));
				}
			}
		}
	}
	
	data_frame = data.frame (proc_algorithm,proc_parameter,proc_names,proc_mq_mean,proc_mq_std,proc_time_mean,proc_time_std);
	names(data_frame) <- c("ALGORITMO", "PARAMETROS","INSTÂNCIA","MQ MEAN","MQ STD", "TIME MEAN", "TIME STD");

	#Gerar HTML com o data_frame
	HTML(data_frame, file = paste(output_path,output_file,sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
}









#process_experiment_data(construtive_folder,"C-data.html");
#process_experiment_data(hc_folder,"HC-data.html");
#process_tabu_experiment_data(tabu_folder,"TS-data.html");
