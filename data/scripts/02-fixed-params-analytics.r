######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### CONFIGURAÇÃO BASE ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

#caminho para os arquivos
base_path = "E:/source_codes/SMC/experiment_dissertacao/";
#base_path = "D:/Marlon/PPGI/SMC/experiment_output/";
#base_path = "D:/Marlon/sources/SMC/experiment_output/";
#base_path = "D:/Marlon/sources/SMC/experiment_output/"; #SBSE03

#experiment_folder = "PARAM_01_INITIAL_SOLUTION_METHOD_SIMPLIFIED";
#experiment_folder = "PARAM_02_REPAIR_METHOD_SIMPLIFIED";
#experiment_folder = "PARAM_03_DESTRUTIVE_METHOD_SIMPLIFIED";
#experiment_folder = "PARAM_04_DESTRUCTION_FACTOR_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_01_MIXED_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_02_MIXED_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_03_MIXED_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_04_MIXED_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_05_MIXED_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_06_MIXED_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_07_MIXED_100_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_08_MIXED_150_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_09_MIXED_200_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_10_MIXED_300_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_11_MIXED_400_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_12_MIXED_500_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_13_MIXED_1000_NO_LIMIT_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_14_RGBIR_010_1000_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_15_RGBI_010_1000_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_16_MIXED_1000_NO_RESTART_NONE_SIMPLIFIED";
#experiment_folder = "ALL_FIXED_17_MIXED_1000_RESTART_FACTOR_0.5_NONE_SIMPLIFIED";
experiment_folder = "ALL_FIXED_18_MIXED_1000_NO_LIMIT_NONE_SIMPLIFIED";
output_folder = "";


#install.packages('plyr');
require(plyr);

#install.packages('R2HTML');
library(R2HTML) #saida para html

#Valores base para os graficos
graph_colors <- c("blue","black","brown","red","pink","purple","gray","gold4","yellow","magenta","navy","orange","orangered","tan1","yellowgreen","rosybrown","red4","palegreen4");
grapth_pch <- c(15,16,17,18,19,23);
default_inset <- -0.2;
small_inset <- -0.25;

INSTANCE_ORDER_18 = c(05,06,11,08,12,02,13,17,07,15,09,01,03,18,14,16,04,10);
INSTANCE_ORDER_122 = c(
			77,122,117,119,37,72,39,74,25,12,18,78,62,109,40,81,22,3,29,34,57
			,33,96,44,120,69,66,79,63,86,70,95,49,68,52,58,53,91,71,19,118,110
			,88,90,56,111,101,92,67,84,94,103,51,23,73,104,64,6,108,99,100,75
			,65,26,116,7,121,107,11,89,113,87,43,36,42,83,24,15,21,85,48,30,20
			,8,13,97,10,105,9,98,59,102,61,115,106,4,31,5,114,45,32,60,14,46,47
			,2,16,1,38,41,112,27,35,28,82,55,50,54,93,80,76,17
		);
INSTANCE_ORDER_124 = c(
			77,123,118,120,37,72,39,74,25,12,18,78,62,109,40,81,22,3,29,34,57
			,33,96,44,121,69,66,79,63,86,70,95,49,68,52,58,53,91,71,19,119,110
			,88,90,56,111,101,92,67,84,94,103,51,23,73,104,64,6,108,99,100,75
			,65,26,117,7,122,107,11,89,113,87,43,36,42,83,24,15,21,85,48,30,20
			,8,13,97,10,105,9,98,59,102,61,115,106,4,31,5,114,45,32,60,14,46,47
			,2,16,1,38,41,112,27,35,28,82,55,50,54,93,80,76,17,116,124
		);


######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### DECLARAÇÃO DAS FUNÇÕES ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 


#function_step_0 - CRIA OS DIRETORIOS DE SAIDA DE DADOS
function_step_0 = function (base_path, experiment_folder, output_folder){
	dir.create(file.path(base_path, output_folder), showWarnings = FALSE);#cria o diretorio de saida padrao
	output_path = paste(base_path,output_folder,"/",sep="");

	dir.create(file.path(output_path, experiment_folder), showWarnings = FALSE);#cria o diretorio de saida da execucao
	output_path = paste(output_path,experiment_folder,"/",sep="");
}


#function_nested_folders_names - VERIFICA AS PASTAS DENTRO DE input_folder E RETORNA O NOME DE CADA UMA
function_nested_folders_names = function(input_path){
	#print(paste("LISTING FOLDERS WITHIN:",input_path));
	flush.console();
	instances_names = list.dirs(input_path, full.names=FALSE);
	return (instances_names[2:length(instances_names)]);
}

#function_find_files - Recupera uma lista com os nomes dos arquivos que estao em uma pasta e que iniciam com prefix
function_find_files = function(input_folder, prefix){
	#print(paste("FILTRANDO ARQUIVOS EM:",input_folder,"COM PREFIXO",prefix));
	file_pattern = paste(prefix,".*.dat",sep="");
	files_names = list.files(path=input_folder, pattern = file_pattern, all.files = FALSE, full.names = FALSE, recursive = FALSE, ignore.case = FALSE, include.dirs = FALSE);
	return(files_names);
}

#function_read_fixed_param_file - Efetua a leitura do aqurivo de dados bruto com os dados do comparativo entre as configurações
function_read_fixed_param_file = function(input_file){
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","character","character","integer","integer","double","double","integer","integer","integer", "integer", "integer")
		#,colClasses=c("character","character","character","integer","integer","double","integer")
		)
	;
	colnames(file_data) <- c("FIXED_PARAM","PARAM_VALUE","CONFIG","N_CONFIG","N_EXECUTION","INITIAL_COST","MQ","ITERATION_MQ","NO_IMPROVEMENT_MAX_GAP","TIME","TOTAL_CLUSTERS","LAST_ITERATION");
	#colnames(file_data) <- c("FIXED_PARAM","PARAM_VALUE","CONFIG","N_CONFIG","N_EXECUTION","MQ","TIME");
	return (file_data);
}




#function_group_execution_data - agrupa os dados da execução, mantendo apenas a mídia de MQ e de tempo
function_group_execution_data = function(instance_data){
	#processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE','CONFIG','N_CONFIG'), function(x) c(
	processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE','CONFIG','N_CONFIG', 'INITIAL_COST'), function(x) c(
		MQ=mean(x$MQ), TIME=mean(x$TIME), MQ_STD=sd(x$MQ), TIME_STD=sd(x$TIME)
		));
	return (processed_data);
}

function_group_last_mq_execution_data = function(instance_data){
	#processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE','CONFIG','N_CONFIG'), function(x) c(
	processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE','CONFIG','N_CONFIG', 'INITIAL_COST'), function(x) c(
		ITERATION_MIN=min(x$ITERATION_MQ), ITERATION_MAX=max(x$ITERATION_MQ), ITERATION_MEAN=mean(x$ITERATION_MQ)
		, LAST_ITERATION_MIN=min(x$LAST_ITERATION), LAST_ITERATION_MAX=max(x$LAST_ITERATION), LAST_ITERATION_MEAN=mean(x$LAST_ITERATION)
		));
	return (processed_data);
}

function_group_no_improvement_gap = function(instance_data){
	#processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE','CONFIG','N_CONFIG'), function(x) c(
	processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE','CONFIG','N_CONFIG', 'INITIAL_COST'), function(x) c(
		NO_IMPROVEMENT_GAP_MIN=min(x$NO_IMPROVEMENT_MAX_GAP), NO_IMPROVEMENT_GAP_MAX=max(x$NO_IMPROVEMENT_MAX_GAP), NO_IMPROVEMENT_GAP_MEAN=mean(x$NO_IMPROVEMENT_MAX_GAP)
		));
	return (processed_data);
}


function_reorder_final_data = function(final_data){
		if(nrow(final_data) == 18){
			final_data = final_data[with(final_data, order(INSTANCE_ORDER_18)), ];
		}else if(nrow(final_data) == 122){
			final_data = final_data[with(final_data, order(INSTANCE_ORDER_122)), ];
		}else if(nrow(final_data) == 124){
			final_data = final_data[with(final_data, order(INSTANCE_ORDER_124)), ];
		}
		return (final_data);
}

#function_group_win_execution_data - agrupa os dados da execuçã, somando cada vitória
function_group_win_execution_data = function(instance_data){
	processed_data = ddply(instance_data, c('FIXED_PARAM','PARAM_VALUE'), function(x) c(
		WIN=sum(x$WIN), TIME_MEAN=mean(x$TIME)
		));
	return (processed_data);
}

function_generate_fixed_params_data = function(base_path, experiment_folder, output_folder){
	input_path = paste(base_path, experiment_folder, sep="");
	fixed_parameters_folders = function_nested_folders_names(input_path);
#current_parameter = fixed_parameters_folders[1];
	for(current_parameter in fixed_parameters_folders){
		parameter_files_folder = paste(input_path, current_parameter, sep="/");
		instance_file_names = function_find_files(parameter_files_folder,"");
		resumed_data = data.frame();
#instance_file_name = instance_file_names[2];
		for(instance_file_name in instance_file_names){
			parameter_file_path = paste(parameter_files_folder, instance_file_name, sep="/");
			instance_data = function_read_fixed_param_file(parameter_file_path);
			grouped_data = function_group_execution_data(instance_data);
			grouped_data = grouped_data[with(grouped_data, order(N_CONFIG, -MQ)), ];#ordenacao
			win_config = c();
			current_config=-1;
			max_mq=-1;
			for(i in 1:nrow(grouped_data)){
				line_data = grouped_data[i,];
				if(line_data$N_CONFIG != current_config){
					current_config=line_data$N_CONFIG;
					max_mq = line_data$MQ;
				}
				win_value=0;
				if(line_data$MQ == max_mq){
					if(line_data$MQ > line_data$INITIAL_COST){
						win_value=1;
					}
				}
				win_config = append(win_config, win_value);
			}
			grouped_data$WIN = win_config;
			resumed_data = rbind (resumed_data, grouped_data);
		}
		grouped_win_data = function_group_win_execution_data(resumed_data);
		grouped_win_data = grouped_win_data[with(grouped_win_data, order(-WIN)), ];
		
		output_path = paste(base_path, output_folder, "/", experiment_folder, "/", sep="");
		dir.create(file.path(output_path), showWarnings = FALSE);#cria o diretorio de saida padrao
		HTML(grouped_win_data, file = paste(output_path,current_parameter,".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
		write.table(grouped_win_data, file = paste(output_path,current_parameter,".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
	}
}

function_generate_final_data = function(base_path, experiment_folder, output_folder){
	input_path = paste(base_path, experiment_folder, sep="");
	final_experiment_folders = function_nested_folders_names(input_path);
#current_parameter = final_experiment_folders[1];
	for(current_parameter in final_experiment_folders){
		parameter_files_folder = paste(input_path, current_parameter, sep="/");
		instance_file_names = function_find_files(parameter_files_folder,"");
		final_data = data.frame();
#		instance_file_name = instance_file_names[2];
		for(instance_file_name in instance_file_names){
			parameter_file_path = paste(parameter_files_folder, instance_file_name, sep="/");
			instance_data = function_read_fixed_param_file(parameter_file_path);
			grouped_data = function_group_execution_data(instance_data);
			grouped_data$INSTANCE =  substr(instance_file_name,1,nchar(instance_file_name)-18);#nome do arquivo sem o final "_1234567890123.dat"
			final_data = rbind (final_data, grouped_data);
		}
		final_data = final_data[c("INSTANCE", "MQ", "MQ_STD", "TIME", "TIME_STD")];#reordena colunas restantes
		
		final_data = function_reorder_final_data(final_data);
		
		output_path = paste(base_path, output_folder, "/", experiment_folder, "/", sep="");
		dir.create(file.path(output_path), showWarnings = FALSE);#cria o diretorio de saida padrao
		HTML(final_data, file = paste(output_path,current_parameter,".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
		write.table(final_data, file = paste(output_path,current_parameter,".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
	}
}

function_generate_last_iteration_mean = function(base_path, experiment_folder, output_folder){
	input_path = paste(base_path, experiment_folder, sep="");
	final_experiment_folders = function_nested_folders_names(input_path);
#current_parameter = final_experiment_folders[1];
	for(current_parameter in final_experiment_folders){
		parameter_files_folder = paste(input_path, current_parameter, sep="/");
		instance_file_names = function_find_files(parameter_files_folder,"");
		final_data = data.frame();
#		instance_file_name = instance_file_names[2];
		for(instance_file_name in instance_file_names){
			parameter_file_path = paste(parameter_files_folder, instance_file_name, sep="/");
			instance_data = function_read_fixed_param_file(parameter_file_path);
			grouped_data = function_group_last_mq_execution_data(instance_data);
			grouped_data$INSTANCE =  substr(instance_file_name,1,nchar(instance_file_name)-18);#nome do arquivo sem o final "_1234567890123.dat"
			final_data = rbind (final_data, grouped_data);
		}
		final_data = final_data[c("INSTANCE", "ITERATION_MIN", "ITERATION_MAX", "ITERATION_MEAN", "LAST_ITERATION_MIN", "LAST_ITERATION_MAX", "LAST_ITERATION_MEAN")];#reordena colunas restantes
		
		final_data = function_reorder_final_data(final_data);
		
		output_path = paste(base_path, output_folder, "/", experiment_folder, "/", sep="");
		dir.create(file.path(output_path), showWarnings = FALSE);#cria o diretorio de saida padrao
		HTML(final_data, file = paste(output_path,current_parameter,"_LAST_ITERATION",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
		write.table(final_data, file = paste(output_path,current_parameter,"_LAST_ITERATION",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
	}
}

function_generate_no_improvement_gap_mean = function(base_path, experiment_folder, output_folder){
	input_path = paste(base_path, experiment_folder, sep="");
	final_experiment_folders = function_nested_folders_names(input_path);
#current_parameter = final_experiment_folders[1];
	for(current_parameter in final_experiment_folders){
		parameter_files_folder = paste(input_path, current_parameter, sep="/");
		instance_file_names = function_find_files(parameter_files_folder,"");
		final_data = data.frame();
#		instance_file_name = instance_file_names[2];
		for(instance_file_name in instance_file_names){
			parameter_file_path = paste(parameter_files_folder, instance_file_name, sep="/");
			instance_data = function_read_fixed_param_file(parameter_file_path);
			grouped_data = function_group_no_improvement_gap(instance_data);
			grouped_data$INSTANCE =  substr(instance_file_name,1,nchar(instance_file_name)-18);#nome do arquivo sem o final "_1234567890123.dat"
			final_data = rbind (final_data, grouped_data);
		}
		final_data = final_data[c("INSTANCE", "NO_IMPROVEMENT_GAP_MIN", "NO_IMPROVEMENT_GAP_MAX", "NO_IMPROVEMENT_GAP_MEAN")];#reordena colunas restantes
		
		final_data = function_reorder_final_data(final_data);
		
		output_path = paste(base_path, output_folder, "/", experiment_folder, "/", sep="");
		dir.create(file.path(output_path), showWarnings = FALSE);#cria o diretorio de saida padrao
		HTML(final_data, file = paste(output_path,current_parameter,"_NO_IMPROVEMENT_GAP",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
		write.table(final_data, file = paste(output_path,current_parameter,"_NO_IMPROVEMENT_GAP",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
	}
}


######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### AUXILIARES DA EXECUÇÃO PRINCIPAL ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

generate_multi_config_data = function (base_path, experiment_folder, output_folder){
	function_step_0(base_path, experiment_folder, output_folder);#cria os diretorios de saida
	function_generate_fixed_params_data(base_path, experiment_folder, output_folder);
}

generate_fixed_config_data = function (base_path, experiment_folder, output_folder){
	function_step_0(base_path, experiment_folder, output_folder);#cria os diretorios de saida
	function_generate_final_data(base_path, experiment_folder, output_folder);
	function_generate_last_iteration_mean(base_path, experiment_folder, output_folder);
	function_generate_no_improvement_gap_mean(base_path, experiment_folder, output_folder);
}


######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### EXECUÇÃO PRINCIPAL ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 
#generate_multi_config_data(base_path, experiment_folder, output_folder);#cria os diretorios de saida
generate_fixed_config_data(base_path, experiment_folder, output_folder);

