#install.packages('R2HTML');
library(R2HTML) #saida para html

#caminho para os arquivos
#path = "C:/Users/kiko/Documents/Codigos Fonte/SMC/";
path = "J:/Marlon/PPGI/SMC/";
output_pdf_path = paste(path,"files/",sep="");
experiment_path = paste(path,"experiment_output",sep="");
alexandre_path = paste(path,"alexandre/",sep="");
pdf_name="graphs.pdf";
pdf2_name="graphs_2.pdf";
pdf3_name="graphs_3.pdf";
pdf4_name="graphs_4.pdf";

table01_name="tabela_1.html";
table02_name="tabela_2.html";

#desenhar os gráficos para o HC
graph_colors <- c(26,51,76,101,126,151);
grapth_pch <- c(15,16,17,18,19,23);
default_inset <- -0.2;
small_inset <- -0.25;


#Ler arquivos do alexandre
alexandre_ga_gga = read.table(paste(alexandre_path,"ga_gga.csv",sep=""), header=TRUE, sep=";", dec=",", fileEncoding="utf8");
alexandre_ga_gne = read.table(paste(alexandre_path,"ga_gne.csv",sep=""), header=TRUE, sep=";", dec=",", fileEncoding="utf8");
alexandre_guloso = read.table(paste(alexandre_path,"guloso.csv",sep=""), header=TRUE, sep=";", dec=",", fileEncoding="utf8");
alexandre_hc = read.table(paste(alexandre_path,"hc.csv",sep=""), header=TRUE, sep=";", dec=",", fileEncoding="utf8");
alexandre_ils = read.table(paste(alexandre_path,"ils.csv",sep=""), header=TRUE, sep=";", dec=",", fileEncoding="utf8");

#criar os pdf
pdf(file = ifelse(TRUE, paste(output_pdf_path,pdf_name,sep="")),7,7);
pdf(file = ifelse(TRUE, paste(output_pdf_path,pdf2_name,sep="")),7,7);
pdf(file = ifelse(TRUE, paste(output_pdf_path,pdf3_name,sep="")),7,7);
pdf(file = ifelse(TRUE, paste(output_pdf_path,pdf4_name,sep="")),7,7);


#meus experimentos
data_file_names = list.files(path=experiment_path, pattern = "*.dat", all.files = FALSE, full.names = FALSE, recursive = FALSE, ignore.case = FALSE, include.dirs = FALSE);


minha_instancia = c();
minha_instancia_mq = c();


table01_01 <- c();
table01_02 <- c();
table01_03 <- c();
table01_04 <- c();
table01_05 <- c();
table01_06 <- c();
table01_07 <- c();
table01_08 <- c();
table01_09 <- c();
table01_10 <- c();
table01_11 <- c();
table01_12 <- c();
table01_13 <- c();

#para cada arquivo de instancia
for(data_file_name in data_file_names) {
	instance_data <- read.table(paste(paste(experiment_path,"/",sep=""),data_file_name,sep=""), header=FALSE, sep=";", dec=",", fileEncoding="utf8");
	colnames(instance_data) <- c("INSTANCIA","TAMANHO","ALGORITMO","PARAMETROS","ITERACAO","CURRENT_MQ","MELHOR_MQ","ITERACAO_MELHOR_MQ","TEMPO","TOTAL_CLUSTERES","MAIOR_CLUSTER","MENOR_CLUSTER","CLUSTERES_ISOLADOS","MENOR_MF","MAIOR_MF");
	instance_name = instance_data$INSTANCIA[1];
	minha_instancia = append(minha_instancia,toString(instance_name));
	minha_instancia_mq = append(minha_instancia_mq, max(instance_data$MELHOR_MQ));
	configurations_executed = levels(instance_data$PARAMETROS);
	
	instance_data$TEMPO = instance_data$TEMPO/1000; #passando para segundos
	
	i=1;
	configurations_max_mq = c();
	configurations_mean_mq = c();
	
	for(current_configuration in configurations_executed) {
		graph_data <- subset(instance_data,PARAMETROS==current_configuration);
		#Gerar o gráfico pelo iteracaoxMQ (multiplas configuracoes)
		x<-0:max(graph_data$ITERACAO);
#		par(xpd=T, mar=par()$mar+c(0,0,0,4));#liberar espaço para legenda
		dev.set(2);
		plot(x, type="n", main=paste(paste(instance_name," : "),graph_data$PARAMETROS[1]), cex.main=0.9, xlab="Iteração", ylab="MQ", ylim=c(0, max(graph_data$MELHOR_MQ)));
		points(graph_data$ITERACAO, graph_data$MELHOR_MQ, type="l", cex = 2, pch=grapth_pch[2], col=graph_colors[2]);
		points(graph_data$ITERACAO, graph_data$CURRENT_MQ, type="l", cex = 2, pch=grapth_pch[1], col=graph_colors[1]);
		
		
		legend('center', inset=default_inset, legend=c("MQ DA ITERAÇÂO","MQ MÁXIMO"), pch="--", col=graph_colors);
#		par(mar=c(5, 4, 4, 2) + 0.1);# Restore default clipping rect
		i=i+1;
		
		configurations_max_mq = append(configurations_max_mq,max(graph_data$MELHOR_MQ));
		configurations_mean_mq = append(configurations_mean_mq,mean(graph_data$CURRENT_MQ));
		
		
		#grafico com dados melhores
		dev.set(5);
		x<-0:max(graph_data$ITERACAO);#todas as iterações no eixo x
		plot(x, type="n", main=paste(paste(instance_name," : "),graph_data$PARAMETROS[1]), cex.main=0.9, xlab="Iteração", ylab="MQ", ylim=c(0, max(graph_data$MELHOR_MQ)));
		
		for(iterarion_best in unique(graph_data$ITERACAO_MELHOR_MQ)){
			rows_mq_changed = subset(graph_data, graph_data$ITERACAO==(iterarion_best+1));
			points(rows_mq_changed$ITERACAO, rows_mq_changed$MELHOR_MQ, type="p", cex = 0.5, pch=grapth_pch[2], col=graph_colors[2]);
		}
		
		#tabela comparativa entre as minhas execucoes
		row_best_mq = subset(graph_data, graph_data$ITERACAO==(max(graph_data$ITERACAO_MELHOR_MQ)+1));
		
		table01_01 <- append(table01_01,toString(instance_name));
		table01_02 <- append(table01_02,row_best_mq$TAMANHO);
		table01_03 <- append(table01_03,toString(current_configuration));
		table01_04 <- append(table01_04,length(unique(graph_data$ITERACAO_MELHOR_MQ))-1);
		table01_05 <- append(table01_05,max(graph_data$ITERACAO_MELHOR_MQ));
		table01_06 <- append(table01_06,max(graph_data$MELHOR_MQ));
		table01_07 <- append(table01_07,max(graph_data$TEMPO));
		table01_08 <- append(table01_08,row_best_mq$TOTAL_CLUSTERES);
		table01_09 <- append(table01_09,row_best_mq$MAIOR_CLUSTER);
		table01_10 <- append(table01_10,row_best_mq$MENOR_CLUSTER);
		table01_11 <- append(table01_11,row_best_mq$CLUSTERES_ISOLADOS);
		table01_12 <- append(table01_12,row_best_mq$MENOR_MF);
		table01_13 <- append(table01_13,row_best_mq$MAIOR_MF);
		
	}
	x<-1:length(configurations_max_mq);
	dev.set(3);
	plot(x, type="n", main=paste("Comparação entre os MQs. Instância: ",instance_name), cex.main=0.9, xlab="Configuração", ylab="MQ", ylim=c(min(configurations_mean_mq), max(configurations_max_mq)), xaxt='n');
	points(x, configurations_max_mq, cex = 2, pch=grapth_pch[1], col=graph_colors[1]);
	points(x, configurations_mean_mq, cex = 2, pch=grapth_pch[2], col=graph_colors[2]);
	legend('bottomleft', legend=levels(graph_data$PARAMETROS), pch="*", col=graph_colors, cex=0.5);

	
	
}
meu_experimento = data.frame (minha_instancia,minha_instancia_mq);
names(meu_experimento) <- c("INSTANCIA", "MQ");

table01_header <- c("INSTANCIA", "TAMANHO", "CONFIGURACAO", "QUANTIDADE DE MELHORAS", "ULTIMA_MELHORIA", "MELHOR_MQ", "TEMPO DE EXECUCAO","TOTAL_CLUSTERES","MAIOR_CLUSTER","MENOR_CLUSTER","CLUSTERES_ISOLADOS", "MENOR_MF", "MAIOR_MF")
table01 <- data.frame(table01_01,table01_02,table01_03,table01_04,table01_05,table01_06,table01_07,table01_08,table01_09,table01_10,table01_11,table01_12,table01_13);
names(table01) <- table01_header;
HTML(table01, file = paste(output_pdf_path,table01_name,sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);



#comparativo com o alexandre
alexandre_melhor_results = c();
alexandre_melhor_tempo = c();
meus_melhores_results = c();
instancias = c();

#alexandre_melhor_results * meus_melhores_results
for(current_instancia in minha_instancia){
	max_current_mq = max(
	      subset(alexandre_ga_gga,INSTANCIA==current_instancia)$MQ
		, subset(alexandre_ga_gne,INSTANCIA==current_instancia)$MQ
		, subset(alexandre_guloso,INSTANCIA==current_instancia)$MQ
		, subset(alexandre_hc,INSTANCIA==current_instancia)$MQ
		, subset(alexandre_ils,INSTANCIA==current_instancia)$MQ
		);
		
	min_curren_time = min(
	      subset(alexandre_ga_gga,INSTANCIA==current_instancia)$TEMPO
		, subset(alexandre_ga_gne,INSTANCIA==current_instancia)$TEMPO
		, subset(alexandre_guloso,INSTANCIA==current_instancia)$TEMPO
		, subset(alexandre_hc,INSTANCIA==current_instancia)$TEMPO
		, subset(alexandre_ils,INSTANCIA==current_instancia)$TEMPO
		);
	meus_melhores_results = append (meus_melhores_results, (subset(meu_experimento,INSTANCIA==current_instancia)$MQ) );
	alexandre_melhor_results = append (alexandre_melhor_results, max_current_mq );
	alexandre_melhor_tempo = append (alexandre_melhor_tempo, min_curren_time );
	instancias = append ( instancias, toString(current_instancia));
}
meus_melhores_results = round(meus_melhores_results, digits=2); #arredonda para duas casas decimais
comparativo_entre_mqs = meus_melhores_results *100 / alexandre_melhor_results;
comparativo_entre_mqs = round(comparativo_entre_mqs, digits=2); #arredonda para duas casas decimais




dev.set(4);
min_alexandre_mq = min(alexandre_ga_gga$MQ, alexandre_ga_gne$MQ, alexandre_guloso$MQ, alexandre_hc$MQ, alexandre_ils$MQ);
max_alexandre_mq = max(alexandre_ga_gga$MQ, alexandre_ga_gne$MQ, alexandre_guloso$MQ, alexandre_hc$MQ, alexandre_ils$MQ);
x <- c(1:length(alexandre_ga_gga$INSTANCIA));
mq_min = min(minha_instancia_mq, min_alexandre_mq );
mq_max = max(minha_instancia_mq, max_alexandre_mq);

par(xpd=T, mar=par()$mar+c(4,0,0,4));#liberar espaço para legenda

plot(x, type="n", main="Comparação entre os MQs obtidos: ", cex.main=0.9, xlab="", ylab="MQ", ylim=c(mq_min, mq_max), xaxt="n");
text(x,rep(min(mq_min)-5,length(x)), srt = 45, adj = 1, labels=alexandre_ga_gga$INSTANCIA, xpd=TRUE);
points(x, alexandre_ga_gga$MQ, type="p", cex = 1, pch=grapth_pch[1], col=graph_colors[1]);
points(x, alexandre_guloso$MQ, type="p", cex = 1, pch=grapth_pch[2], col=graph_colors[2]);
points(x, alexandre_hc$MQ, type="p", cex = 1, pch=grapth_pch[3], col=graph_colors[3]);
points(x, alexandre_ils$MQ, type="p", cex = 1, pch=grapth_pch[4], col=graph_colors[4]);

points(x, meus_melhores_results, type="p", cex = 1, pch=grapth_pch[5], col=graph_colors[5]);

points(x, alexandre_ga_gne$MQ, type="p", cex = 1, pch=grapth_pch[6], col=graph_colors[6]);
legend('right',  inset=c(mq_min-1,0), legend=c("G_GGA", "Guloso", "HC", "ILS", "TABU", "G_GNE"), pch=grapth_pch, col=graph_colors, cex=0.8);

par(mar=c(5, 4, 4, 2) + 0.1);# Restore default clipping rect






dev.set(4);
x <- c(1:length(instancias));
comp_min = min(comparativo_entre_mqs);
comp_max = max(comparativo_entre_mqs);

par(xpd=T, mar=par()$mar+c(4,0,0,4));#liberar espaço para legenda

plot(x, type="n", main="Comparação entre os MQs obtidos: ", cex.main=0.9, xlab="", ylab="MQ", ylim=c(comp_min, comp_max), xaxt="n");
text(x,rep(99.4,length(x)), srt = 45, adj = 1, labels=alexandre_ga_gga$INSTANCIA, xpd=TRUE);
points(x, comparativo_entre_mqs, type="p", cex = 1, pch=grapth_pch[5], col=graph_colors[1]);
text(comparativo_entre_mqs, srt = 45, adj = 1, labels=comparativo_entre_mqs, xpd=TRUE, pos=3);
legend('right',  inset=c(comp_min-1,0), legend=c("Comparativo"), pch=grapth_pch[5], col=graph_colors[1], cex=0.8);

par(mar=c(5, 4, 4, 2) + 0.1);# Restore default clipping rect


dev.off(); #fecha o pdf
dev.off(); #fecha o pdf
dev.off(); #fecha o pdf
dev.off(); #fecha o pdf



table02_header <- table01_header;
table02_header <- append(table02_header,"MQ_ALEXANDRE");
table02_header <- append(table02_header,"TEMPO_ALEXANDRE");
table02 <- data.frame(table01_01,table01_02,table01_03,table01_04,table01_05,table01_06,table01_07,table01_08,table01_09,table01_10,table01_11,table01_12,table01_13,alexandre_melhor_results,alexandre_melhor_tempo);
names(table02) <- table02_header;
HTML(table02, file = paste(output_pdf_path,table02_name,sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);

