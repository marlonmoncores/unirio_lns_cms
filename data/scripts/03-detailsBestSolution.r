######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### CONFIGURAÇÃO BASE ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

#caminho para os arquivos
base_path = "E:/source_codes/SMC/experiment_dissertacao/";
#base_path = "D:/Marlon/PPGI/SMC/experiment_output/";
base_path = "D:/Marlon/source_codes/SMC/experiment_dissertacao/"; #Vassouras
#base_path = "D:/Marlon/sources/SMC/experiment_output/"; #SBSE03

experiment_folder = "ALL_FIXED_18_MIXED_1000_NO_LIMIT_DETAILS_NONE_SIMPLIFIED";
output_folder = "";


#install.packages('plyr');
require(plyr);

#install.packages('R2HTML');
library(R2HTML) #saida para html

#Valores base para os graficos
graph_colors <- c("blue","black","brown","red","pink","purple","gray","gold4","yellow","magenta","navy","orange","orangered","tan1","yellowgreen","rosybrown","red4","palegreen4");
grapth_pch <- c(15,16,17,18,19,23);
default_inset <- -0.2;
small_inset <- -0.25;

INSTANCE_ORDER_124 = c(
			77,123,118,120,37,72,39,74,25,12,18,78,62,109,40,81,22,3,29,34,57
			,33,96,44,121,69,66,79,63,86,70,95,49,68,52,58,53,91,71,19,119,110
			,88,90,56,111,101,92,67,84,94,103,51,23,73,104,64,6,108,99,100,75
			,65,26,117,7,122,107,11,89,113,87,43,36,42,83,24,15,21,85,48,30,20
			,8,13,97,10,105,9,98,59,102,61,115,106,4,31,5,114,45,32,60,14,46,47
			,2,16,1,38,41,112,27,35,28,82,55,50,54,93,80,76,17,116,124
		);


######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### DECLARAÇÃO DAS FUNÇÕES ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 


#function_step_0 - CRIA OS DIRETORIOS DE SAIDA DE DADOS
function_step_0 = function (base_path, experiment_folder, output_folder){
	dir.create(file.path(base_path, output_folder), showWarnings = FALSE);#cria o diretorio de saida padrao
	output_path = paste(base_path,output_folder,"/",sep="");

	dir.create(file.path(output_path, experiment_folder), showWarnings = FALSE);#cria o diretorio de saida da execucao
	output_path = paste(output_path,experiment_folder,"/",sep="");
}


#function_nested_folders_names - VERIFICA AS PASTAS DENTRO DE input_folder E RETORNA O NOME DE CADA UMA
function_nested_folders_names = function(input_path){
	#print(paste("LISTING FOLDERS WITHIN:",input_path));
	flush.console();
	instances_names = list.dirs(input_path, full.names=FALSE);
	return (instances_names[2:length(instances_names)]);
}

#function_find_files - Recupera uma lista com os nomes dos arquivos que estao em uma pasta e que iniciam com prefix
function_find_files = function(input_folder, prefix){
	#print(paste("FILTRANDO ARQUIVOS EM:",input_folder,"COM PREFIXO",prefix));
	file_pattern = paste(prefix,".*.dat",sep="");
	files_names = list.files(path=input_folder, pattern = file_pattern, all.files = FALSE, full.names = FALSE, recursive = FALSE, ignore.case = FALSE, include.dirs = FALSE);
	return(files_names);
}

#function_read_fixed_detail_param_file - Efetua a leitura do aqurivo de dados bruto com os dados do comparativo entre as configurações
function_read_fixed_detail_param_file = function(input_file){
	file_data = read.table(input_file, header=FALSE, sep=";", dec=".", fileEncoding="utf8"
		,colClasses=c("character","character","character","integer","integer","double","double","integer","integer","integer", "integer", "integer"
		,"integer","integer","integer","double","double","character")
		);
	colnames(file_data) <- c("FIXED_PARAM","PARAM_VALUE","CONFIG","N_CONFIG","N_EXECUTION","INITIAL_COST","MQ","ITERATION_MQ","NO_IMPROVEMENT_MAX_GAP","TIME","TOTAL_CLUSTERS","LAST_ITERATION"
	,"BIGGEST_CLUSTER","SMALLEST_CLUSTER","ISOLATED_CLUSTERS","LOWER_CLUSTER_MF","HIGHER_CLUSTER_MF","SOLUTION");
	return (file_data);
}

function_reorder_final_data = function(final_data){
		if(nrow(final_data) == 18){
			final_data = final_data[with(final_data, order(INSTANCE_ORDER_18)), ];
		}else if(nrow(final_data) == 122){
			final_data = final_data[with(final_data, order(INSTANCE_ORDER_122)), ];
		}else if(nrow(final_data) == 124){
			final_data = final_data[with(final_data, order(INSTANCE_ORDER_124)), ];
		}
		return (final_data);
}





######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### AUXILIARES DA EXECUÇÃO PRINCIPAL ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 

find_best_line_on_data = function(instance_data){
	best_line = instance_data[1,];
	for(i in 1:nrow(instance_data)){
		row_data=instance_data[i,];
		if(row_data$MQ > best_line$MQ){
			best_line = row_data;
		}
	}
	return(best_line);
}

generate_detail_data = function (base_path, experiment_folder, output_folder){
	function_step_0(base_path, experiment_folder, output_folder);#cria os diretorios de saida
	
	input_path = paste(base_path, experiment_folder, sep="");
	final_experiment_folders = function_nested_folders_names(input_path);
	current_folder = final_experiment_folders[1];
	for(current_folder in final_experiment_folders){
		parameter_files_folder = paste(input_path, current_folder, sep="/");
		instance_file_names = function_find_files(parameter_files_folder,"");
		final_data = data.frame();
		instance_file_name = instance_file_names[2];
		for(instance_file_name in instance_file_names){
			parameter_file_path = paste(parameter_files_folder, instance_file_name, sep="/");
			instance_data = function_read_fixed_detail_param_file(parameter_file_path);
			best_row_on_instance=find_best_line_on_data(instance_data);
			best_row_on_instance$INSTANCE =  substr(instance_file_name,1,nchar(instance_file_name)-18);#nome do arquivo sem o final "_1234567890123.dat"
			final_data = rbind (final_data, best_row_on_instance);
		}
		final_data = function_reorder_final_data(final_data);
		final_data = final_data[c("INSTANCE", "MQ", "ITERATION_MQ", "ITERATION_MQ","TOTAL_CLUSTERS","BIGGEST_CLUSTER","SMALLEST_CLUSTER","ISOLATED_CLUSTERS","LOWER_CLUSTER_MF","HIGHER_CLUSTER_MF","SOLUTION")];
		
		output_path = paste(base_path, output_folder, "/", experiment_folder, "/", sep="");
		dir.create(file.path(output_path), showWarnings = FALSE);#cria o diretorio de saida padrao
		HTML(final_data, file = paste(output_path,"details",".html",sep=""), Border = 0, innerBorder=1, append=FALSE, digits=5);
		write.table(final_data, file = paste(output_path,"details",".csv",sep=""),row.names=FALSE, na="",col.names=FALSE, sep=";");
	}
	
}


######### ######### ######### ######### ######### ######### ######### ######### ######### #########   
######### ######### ######### ######### ######### EXECUÇÃO PRINCIPAL ######### ######### ######### ######### ######### 
######### ######### ######### ######### ######### ######### ######### ######### ######### ######### 
generate_detail_data(base_path, experiment_folder, output_folder);




