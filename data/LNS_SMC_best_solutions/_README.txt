Each file represents the best solution found by LNS_CMS method.
The algorithms' output has only the classes that are not pruned by the pre-processing step.
Each file has 3 columns
INDEX	CLUSTER_#	MODULE_NAME