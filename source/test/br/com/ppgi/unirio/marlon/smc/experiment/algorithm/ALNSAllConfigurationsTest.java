package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.mdg.simplifier.MDGSimplifier;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.alns.ALNSConfiguration;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.alns.AdaptativeLargeNeighborhoodSearch;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.alns.ALNSConfigurationBuilder;

public class ALNSAllConfigurationsTest extends ExperimentBase{
    
    public ALNSAllConfigurationsTest(){
        RUN_TIMES = 3;
    }

    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        int sizeBefore = mdg.getSize();
        MDGSimplifier.simplify(mdg);//simplificar MDG
        System.out.println("TAMANHO DE: "+sizeBefore+" PARA: "+mdg.getSize());

        for (int configN=0;configN<ALNSConfigurationBuilder.calculateTotalConfigurationsAvailable();configN++){
            for (int i = 1; i <= RUN_TIMES; i++) {//para cada vez desejada
                ALNSConfiguration config = ALNSConfigurationBuilder.buildConfiguration(configN, mdg, OUTPUT_TO,beginTestTimestamp+"",configN+"_"+i);
                
                long time = System.currentTimeMillis();

                AdaptativeLargeNeighborhoodSearch alns = new AdaptativeLargeNeighborhoodSearch(config);
                alns.execute();//executa o algoritmo

                time = System.currentTimeMillis()-time;
                System.out.println(mdg.getName()+";"+config.toString()+";"+i+";"+time);

                config.close();

                if(i< RUN_TIMES){//nao roda na ultima vez
                    afterEachTime();
                }
            }
        }
        return null;
    }

    
    @Override
    protected String testName() {
        return "ALNSAll";
    }

}
