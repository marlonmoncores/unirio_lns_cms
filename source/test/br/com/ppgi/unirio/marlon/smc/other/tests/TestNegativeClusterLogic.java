/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ppgi.unirio.marlon.smc.other.tests;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceParseException;
import br.com.ppgi.unirio.marlon.smc.mdg.ClusterMetrics;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveAglomerativeMQ;
import java.io.File;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Marlon Monçores
 */
public class TestNegativeClusterLogic extends ExperimentBase{

    
    @Test
    @Override
    public void runExperiment() throws InstanceParseException, IOException{
        File[] instances = INSTANCE_WORKER.retrieveAllInstanceFiles();//leitura das instancias
        //for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < TOTAL_INSTANCE;index++){//para cada instancia
        for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < 1;index++){//para cada instancia
            ModuleDependencyGraph mdg = INSTANCE_WORKER.readInstanceFile(instances[index]);
            runAlgorithm(mdg);
        }
    }
    
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        System.out.println(mdg.getName()+";"+mdg.getSize()+";"+mdg.getTotalDependencyCount());
        ClusterMetrics cm1 = new ClusterMetrics(mdg, new ConstrutiveAglomerativeMQ().createSolution(mdg));
        ClusterMetrics cm2 = new ClusterMetrics(mdg, new ConstrutiveAglomerativeMQ().createSolution(mdg));
        
        
        System.out.println("MQ-INICIO: "+cm1.calculateMQ()+" # "+cm2.calculateMQ() +" # "+ cm1.getSolutionAsString() + " # "+cm1.getClustersStatusAsString());
        
        int[] clusteresToMove = {0,1};
        int[] whereToMoveClus = {3,5};
        
        for(int i=0;i< clusteresToMove.length;i++){
            makeMoviment(cm1, cm2, clusteresToMove[i], whereToMoveClus[i]);
            System.out.println("MQ-FIM: "+cm1.calculateMQ()+" # "+cm2.calculateMQ() +" # "+ cm1.getSolutionAsString() + " # "+cm1.getClustersStatusAsString());
    }
        
        return null;
    }
    
    private void makeMoviment(ClusterMetrics cm1, ClusterMetrics cm2, int module, int toCluster){
        cm1.makeMoviment(module, -1);
        System.out.println("MQ-MEIO: "+cm1.calculateMQ()+" # "+ cm1.getSolutionAsString() + " # "+cm1.getClustersStatusAsString());
        cm1.makeMoviment(module, toCluster);
        cm2.makeMoviment(module, toCluster);
    }

    @Override
    protected String testName() {
        return "Exibir configurações das instâncias";
    }
    
    
    
}
