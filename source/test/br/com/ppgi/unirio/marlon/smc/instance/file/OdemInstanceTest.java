package br.com.ppgi.unirio.marlon.smc.instance.file;
import br.com.ppgi.unirio.marlon.smc.instance.file.odem.OdemInstanceFileWorker;
import java.io.File;
import org.junit.Test;
import sobol.problems.clustering.generic.model.Project;

public class OdemInstanceTest{

	private OdemInstanceFileWorker instanceWorker = new OdemInstanceFileWorker();
	
        /**
         * Executa a leitura de todas as instancias do tipo odem
         * @throws InstanceParseException 
         */
	@Test
	public void readAllInstances () throws InstanceParseException{
		File[] instanceFiles = instanceWorker.retrieveAllInstanceFiles();
					
		for(File currentInstance: instanceFiles){
			readInstanceFile(currentInstance);
		}		
	}
        
        /**
         * Efetua a leitura de uma instancia no formato odem e escreve no console alguns dados sobre a mesma
         * @param currentInstance
         * @throws InstanceParseException 
         */
        private void readInstanceFile(File currentInstance) throws InstanceParseException {
		System.out.println("INSTANCE START: "+ currentInstance.getName());
		
		Project project = instanceWorker.readInstanceFile(currentInstance);
		
		System.out.println("INSTANCE NAME: "+project.getName());
		System.out.println("INSTANCE PACKAGES COUNT: "+project.getPackageCount());
		System.out.println("INSTANCE CLASSES COUNT: "+project.getClassCount());
		System.out.println("INSTANCE DEPENDENCY COUNT: "+project.getDependencyCount());
		
		System.out.println("INSTANCE END: "+ currentInstance.getName());
                System.out.println("------------- ---------------- ---------------");
	}
}
