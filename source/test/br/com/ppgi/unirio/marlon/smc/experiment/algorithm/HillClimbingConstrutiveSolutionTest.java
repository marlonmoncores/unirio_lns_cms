package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveAglomerativeMQ;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.hc.HillClimbing;

public class HillClimbingConstrutiveSolutionTest extends HillClimbingTest{
	
        
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        int[] construtiveSolution = new ConstrutiveAglomerativeMQ().createSolution(mdg);
        HillClimbing hc = new HillClimbing(out, "");
        return hc.execute(mdg, construtiveSolution, THRESHOLD);
    }
}
