/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ppgi.unirio.marlon.smc.other.tests;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceParseException;
import br.com.ppgi.unirio.marlon.smc.mdg.ClusterMetrics;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.mdg.simplifier.MDGSimplifier;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.junit.Test;

/**
 * This class recieve a solution and calculate it's MQ Value
 * @author Marlon Monçores
 */
public class ILSSolutionCalculatorFix extends ExperimentBase{
    
    private static final Map<String, Integer> INSTACE_GROUPS;
    private static final Map<String, Integer> INSTACE_ORDER;
    
    static{
        INSTACE_GROUPS = new HashMap<>();
        INSTACE_ORDER = new HashMap<>();
        
        INSTACE_GROUPS.put("small",1);INSTACE_ORDER.put("small",2);
INSTACE_GROUPS.put("compiler",1);INSTACE_ORDER.put("compiler",3);
INSTACE_GROUPS.put("jstl",1);INSTACE_ORDER.put("jstl",6);
INSTACE_GROUPS.put("lab4",1);INSTACE_ORDER.put("lab4",7);
INSTACE_GROUPS.put("nos",1);INSTACE_ORDER.put("nos",10);
INSTACE_GROUPS.put("lslayout",1);INSTACE_ORDER.put("lslayout",11);
INSTACE_GROUPS.put("boxer",1);INSTACE_ORDER.put("boxer",12);
INSTACE_GROUPS.put("mtunis",1);INSTACE_ORDER.put("mtunis",15);
INSTACE_GROUPS.put("bunch",1);INSTACE_ORDER.put("bunch",18);
INSTACE_GROUPS.put("ispell",1);INSTACE_ORDER.put("ispell",19);
INSTACE_GROUPS.put("nanoxml",1);INSTACE_ORDER.put("nanoxml",21);
INSTACE_GROUPS.put("ciald",1);INSTACE_ORDER.put("ciald",22);
INSTACE_GROUPS.put("jodamoney",1);INSTACE_ORDER.put("jodamoney",23);
INSTACE_GROUPS.put("modulizer",1);INSTACE_ORDER.put("modulizer",24);
INSTACE_GROUPS.put("jxlsreader",1);INSTACE_ORDER.put("jxlsreader",26);
INSTACE_GROUPS.put("rcs",1);INSTACE_ORDER.put("rcs",31);
INSTACE_GROUPS.put("seemp",1);INSTACE_ORDER.put("seemp",32);
INSTACE_GROUPS.put("apache_zip",1);INSTACE_ORDER.put("apache_zip",37);
INSTACE_GROUPS.put("star",1);INSTACE_ORDER.put("star",38);
INSTACE_GROUPS.put("bison",1);INSTACE_ORDER.put("bison",39);
INSTACE_GROUPS.put("dot",1);INSTACE_ORDER.put("dot",44);
INSTACE_GROUPS.put("udt-java",1);INSTACE_ORDER.put("udt-java",55);
INSTACE_GROUPS.put("javaocr",1);INSTACE_ORDER.put("javaocr",56);
INSTACE_GROUPS.put("pfcda_base",1);INSTACE_ORDER.put("pfcda_base",59);
INSTACE_GROUPS.put("servletapi",1);INSTACE_ORDER.put("servletapi",60);
INSTACE_GROUPS.put("bunch2",1);INSTACE_ORDER.put("bunch2",62);
INSTACE_GROUPS.put("forms",1);INSTACE_ORDER.put("forms",63);
INSTACE_GROUPS.put("jscatterplot",1);INSTACE_ORDER.put("jscatterplot",64);
INSTACE_GROUPS.put("jxlscore",2);INSTACE_ORDER.put("jxlscore",65);
INSTACE_GROUPS.put("jfluid",2);INSTACE_ORDER.put("jfluid",67);
INSTACE_GROUPS.put("grappa",2);INSTACE_ORDER.put("grappa",68);
INSTACE_GROUPS.put("jpassword",2);INSTACE_ORDER.put("jpassword",73);
INSTACE_GROUPS.put("junit",2);INSTACE_ORDER.put("junit",75);
INSTACE_GROUPS.put("bunch_2",2);INSTACE_ORDER.put("bunch_2",78);
INSTACE_GROUPS.put("xmldom",2);INSTACE_ORDER.put("xmldom",80);
INSTACE_GROUPS.put("cia++",2);INSTACE_ORDER.put("cia++",81);
INSTACE_GROUPS.put("tinytim",2);INSTACE_ORDER.put("tinytim",82);
INSTACE_GROUPS.put("jkaryoscope",2);INSTACE_ORDER.put("jkaryoscope",84);
INSTACE_GROUPS.put("gae_plugin_core",2);INSTACE_ORDER.put("gae_plugin_core",86);
INSTACE_GROUPS.put("javacc",2);INSTACE_ORDER.put("javacc",88);
INSTACE_GROUPS.put("lucent",2);INSTACE_ORDER.put("lucent",89);
INSTACE_GROUPS.put("javageom",2);INSTACE_ORDER.put("javageom",90);
INSTACE_GROUPS.put("incl",2);INSTACE_ORDER.put("incl",91);
INSTACE_GROUPS.put("jdendogram",2);INSTACE_ORDER.put("jdendogram",92);
INSTACE_GROUPS.put("xmlapi",2);INSTACE_ORDER.put("xmlapi",93);
INSTACE_GROUPS.put("jmetal",3);INSTACE_ORDER.put("jmetal",94);
INSTACE_GROUPS.put("dom4j",3);INSTACE_ORDER.put("dom4j",96);
INSTACE_GROUPS.put("pdf_renderer",3);INSTACE_ORDER.put("pdf_renderer",98);
INSTACE_GROUPS.put("jung_graph_model",3);INSTACE_ORDER.put("jung_graph_model",99);
INSTACE_GROUPS.put("jung_visualization",3);INSTACE_ORDER.put("jung_visualization",100);
INSTACE_GROUPS.put("jconsole",3);INSTACE_ORDER.put("jconsole",101);
INSTACE_GROUPS.put("pfcda_swing",3);INSTACE_ORDER.put("pfcda_swing",102);
INSTACE_GROUPS.put("jml-1.0b4",3);INSTACE_ORDER.put("jml-1.0b4",103);
INSTACE_GROUPS.put("jpassword2",3);INSTACE_ORDER.put("jpassword2",104);
INSTACE_GROUPS.put("notelab-full",3);INSTACE_ORDER.put("notelab-full",105);
INSTACE_GROUPS.put("poormans cms",3);INSTACE_ORDER.put("poormans cms",106);
INSTACE_GROUPS.put("log4j",3);INSTACE_ORDER.put("log4j",107);
INSTACE_GROUPS.put("jtreeview",3);INSTACE_ORDER.put("jtreeview",108);
INSTACE_GROUPS.put("bunchall",3);INSTACE_ORDER.put("bunchall",109);
INSTACE_GROUPS.put("jace",3);INSTACE_ORDER.put("jace",110);
INSTACE_GROUPS.put("javaws",3);INSTACE_ORDER.put("javaws",111);
INSTACE_GROUPS.put("swing",4);INSTACE_ORDER.put("swing",112);
INSTACE_GROUPS.put("lwjgl-2.8.4",4);INSTACE_ORDER.put("lwjgl-2.8.4",113);
INSTACE_GROUPS.put("res_cobol",4);INSTACE_ORDER.put("res_cobol",114);
INSTACE_GROUPS.put("y_base",4);INSTACE_ORDER.put("y_base",116);
INSTACE_GROUPS.put("apache_ant_taskdef",4);INSTACE_ORDER.put("apache_ant_taskdef",118);
INSTACE_GROUPS.put("itextpdf",4);INSTACE_ORDER.put("itextpdf",119);
INSTACE_GROUPS.put("apache_lucene_core",4);INSTACE_ORDER.put("apache_lucene_core",120);
INSTACE_GROUPS.put("eclipse_jgit",4);INSTACE_ORDER.put("eclipse_jgit",121);
INSTACE_GROUPS.put("apache_ant",4);INSTACE_ORDER.put("apache_ant",123);
INSTACE_GROUPS.put("ylayout",4);INSTACE_ORDER.put("ylayout",124);
INSTACE_GROUPS.put("squid",1);INSTACE_ORDER.put("squid",1);
INSTACE_GROUPS.put("random",1);INSTACE_ORDER.put("random",4);
INSTACE_GROUPS.put("regexp",1);INSTACE_ORDER.put("regexp",5);
INSTACE_GROUPS.put("netkit-ping",1);INSTACE_ORDER.put("netkit-ping",8);
INSTACE_GROUPS.put("nss_ldap",1);INSTACE_ORDER.put("nss_ldap",9);
INSTACE_GROUPS.put("netkit-tftpd",1);INSTACE_ORDER.put("netkit-tftpd",13);
INSTACE_GROUPS.put("sharutils",1);INSTACE_ORDER.put("sharutils",14);
INSTACE_GROUPS.put("spdb",1);INSTACE_ORDER.put("spdb",16);
INSTACE_GROUPS.put("xtell",1);INSTACE_ORDER.put("xtell",17);
INSTACE_GROUPS.put("netkit-inetd",1);INSTACE_ORDER.put("netkit-inetd",20);
INSTACE_GROUPS.put("bootp",1);INSTACE_ORDER.put("bootp",25);
INSTACE_GROUPS.put("sysklogd-1",1);INSTACE_ORDER.put("sysklogd-1",27);
INSTACE_GROUPS.put("telnetd",1);INSTACE_ORDER.put("telnetd",28);
INSTACE_GROUPS.put("crond",1);INSTACE_ORDER.put("crond",29);
INSTACE_GROUPS.put("netkit-ftp",1);INSTACE_ORDER.put("netkit-ftp",30);
INSTACE_GROUPS.put("dhcpd-2",1);INSTACE_ORDER.put("dhcpd-2",33);
INSTACE_GROUPS.put("cyrus-sasl",1);INSTACE_ORDER.put("cyrus-sasl",34);
INSTACE_GROUPS.put("tcsh",1);INSTACE_ORDER.put("tcsh",35);
INSTACE_GROUPS.put("micq",1);INSTACE_ORDER.put("micq",36);
INSTACE_GROUPS.put("cia",1);INSTACE_ORDER.put("cia",40);
INSTACE_GROUPS.put("stunnel",1);INSTACE_ORDER.put("stunnel",41);
INSTACE_GROUPS.put("minicom",1);INSTACE_ORDER.put("minicom",42);
INSTACE_GROUPS.put("mailx",1);INSTACE_ORDER.put("mailx",43);
INSTACE_GROUPS.put("screen",1);INSTACE_ORDER.put("screen",45);
INSTACE_GROUPS.put("slang",1);INSTACE_ORDER.put("slang",46);
INSTACE_GROUPS.put("slrn",1);INSTACE_ORDER.put("slrn",47);
INSTACE_GROUPS.put("net-tools",1);INSTACE_ORDER.put("net-tools",48);
INSTACE_GROUPS.put("graph10up49",1);INSTACE_ORDER.put("graph10up49",49);
INSTACE_GROUPS.put("wu-ftpd-1",1);INSTACE_ORDER.put("wu-ftpd-1",50);
INSTACE_GROUPS.put("joe",1);INSTACE_ORDER.put("joe",51);
INSTACE_GROUPS.put("hw",1);INSTACE_ORDER.put("hw",52);
INSTACE_GROUPS.put("imapd-1",1);INSTACE_ORDER.put("imapd-1",53);
INSTACE_GROUPS.put("wu-ftpd-3",1);INSTACE_ORDER.put("wu-ftpd-3",54);
INSTACE_GROUPS.put("dhcpd-1",1);INSTACE_ORDER.put("dhcpd-1",57);
INSTACE_GROUPS.put("icecast",1);INSTACE_ORDER.put("icecast",58);
INSTACE_GROUPS.put("php",1);INSTACE_ORDER.put("php",61);
INSTACE_GROUPS.put("elm-2",2);INSTACE_ORDER.put("elm-2",66);
INSTACE_GROUPS.put("elm-1",2);INSTACE_ORDER.put("elm-1",69);
INSTACE_GROUPS.put("gnupg",2);INSTACE_ORDER.put("gnupg",70);
INSTACE_GROUPS.put("inn",2);INSTACE_ORDER.put("inn",71);
INSTACE_GROUPS.put("bash",2);INSTACE_ORDER.put("bash",72);
INSTACE_GROUPS.put("bitchx",2);INSTACE_ORDER.put("bitchx",74);
INSTACE_GROUPS.put("xntp",2);INSTACE_ORDER.put("xntp",76);
INSTACE_GROUPS.put("acqcigna",2);INSTACE_ORDER.put("acqcigna",77);
INSTACE_GROUPS.put("exim",2);INSTACE_ORDER.put("exim",79);
INSTACE_GROUPS.put("mod_ssl",2);INSTACE_ORDER.put("mod_ssl",83);
INSTACE_GROUPS.put("ncurses",2);INSTACE_ORDER.put("ncurses",85);
INSTACE_GROUPS.put("lynx",2);INSTACE_ORDER.put("lynx",87);
INSTACE_GROUPS.put("graph10up193",3);INSTACE_ORDER.put("graph10up193",95);
INSTACE_GROUPS.put("nmh",3);INSTACE_ORDER.put("nmh",97);
INSTACE_GROUPS.put("ping_libc",4);INSTACE_ORDER.put("ping_libc",115);
INSTACE_GROUPS.put("krb5",4);INSTACE_ORDER.put("krb5",117);
INSTACE_GROUPS.put("linux",4);INSTACE_ORDER.put("linux",122);

    }
    
    public ILSSolutionCalculatorFix(){
        this.BEGIN_INSTANCE = 0;//66;//31;
        //this.TOTAL_INSTANCE = 1;
    }
    
    @Test
    @Override
    public void runExperiment() throws InstanceParseException, IOException{
        Map<String, InstanceData> solutionsILS = readSolutionsFile("../data/ILS_RAW_DATA_W_MQ_PROBLEM.txt");
        
        File[] instances = INSTANCE_WORKER.retrieveAllInstanceFiles();//leitura das instancias
        Map<String, ModuleDependencyGraph> mdgs = new HashMap<>();
        for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < TOTAL_INSTANCE;index++){//para cada instancia
            ModuleDependencyGraph mdg = INSTANCE_WORKER.readInstanceFile(instances[index]);
            String name = mdg.getName().toLowerCase();
            mdgs.put(name, mdg);
        }
        
        for (Map.Entry<String, InstanceData> entry : solutionsILS.entrySet()) {
            ModuleDependencyGraph mdg = mdgs.get(entry.getKey());
            if(mdg == null) System.out.println(entry.getKey());
//            if(!mdg.getName().equalsIgnoreCase("hw"))continue;
            calculateILSMQData(mdg, entry.getValue());
        }
        
        
//        printILSData(solutionsILS);
        printBestILSSolution(solutionsILS);
    }
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    private static class InstanceCycleData{
        public int cycleNumber;
        public int time;
        public double mq;
        public double new_mq;
        public int[] solution;
    }
    
    private static class InstanceData{
        public String name;
        public int order;
        public int group;
        public List<InstanceCycleData> cycleData;
        
        public InstanceData(String name){
            this.name=name;
            this.cycleData = new ArrayList<>();
            this.order = INSTACE_ORDER.get(name);
            this.group = INSTACE_GROUPS.get(name);
        }
    }
    
    private Map<String, InstanceData> readSolutionsFile(String filePath) throws FileNotFoundException{
        Map<String, InstanceData> returnData = new LinkedHashMap<>();
        File file = new File(filePath);
        Scanner in = new Scanner(file);
        int totalCycles=100;
        
        while(in.hasNextLine()) {
            String instance = in.nextLine().split("=")[1].toLowerCase();
            InstanceData instanceData = new InstanceData(instance);
            in.nextLine();
            in.nextLine();
            for (int i = 0; i < totalCycles; i++) {
                String[] cycleLine = in.nextLine().split(";");
                InstanceCycleData cycleData = new InstanceCycleData();
                cycleData.cycleNumber = Integer.parseInt(cycleLine[0].split("#")[1].trim());
                cycleData.time = Integer.parseInt(cycleLine[1].trim());
                cycleData.mq = Double.parseDouble(cycleLine[2].trim().replace("-", "").replace(",", "."));
                
                String[] solutionT = cycleLine[4].split(",");
                int[] solution = new int[solutionT.length];
                for(int j=0;j<solutionT.length;j++){
                    solution[j] = Integer.parseInt(solutionT[j].trim());
                }
                cycleData.solution = solution;
                instanceData.cycleData.add(cycleData);
            }
            for (int i = 0; i < 7; i++) {
                in.nextLine();//pula as linhas com o resumo
            }
            returnData.put(instance, instanceData);
        }
        return returnData;
    }
    
   
    private void calculateILSMQData(ModuleDependencyGraph mdg, InstanceData instanceData){
        for (InstanceCycleData instanceCycleData : instanceData.cycleData) {
            ClusterMetrics cm = new ClusterMetrics(mdg, instanceCycleData.solution);
            double newMQ = cm.calculateMQ(); 
            instanceCycleData.new_mq = newMQ;
        }
    }
    
    
    private void printILSData(Map<String, InstanceData> solutionsILS){
        System.out.println("INSTANCE;GROUP;ORDER;SOLVER;MQ;TIME");
        for (InstanceData instanceData : solutionsILS.values()) {
            String name=instanceData.name;
            int group=instanceData.group;
            int order=instanceData.order;
            String solver="ILS";
            for (InstanceCycleData instanceCycleData : instanceData.cycleData) {
                double mq = instanceCycleData.new_mq;
                int time = instanceCycleData.time;
                System.out.println(name+";"+group+";"+order+";"+solver+";"+mq+";"+time);
            }
        }
    }
    
    private void printBestILSSolution(Map<String, InstanceData> solutionsILS){
        System.out.println("INSTANCE;SOLUTION;MQ");
        for (InstanceData instanceData : solutionsILS.values()) {
            String name=instanceData.name;
//            if(name.equalsIgnoreCase("hw"))continue;
            int[] best_solution=null;
            double best_mq = 0;
            for (InstanceCycleData instanceCycleData : instanceData.cycleData) {
                double mq = instanceCycleData.new_mq;
                if(mq > best_mq){
                    best_mq = mq;
                    best_solution = instanceCycleData.solution;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append(best_solution[0]);
            for (int i = 1; i < best_solution.length; i++) {
                sb.append(",");
                sb.append(best_solution[i]);
            }
            System.out.println(name+";"+sb.toString()+";"+best_mq);
//            System.out.println(name+";"+best_mq);
        }
    }
    

    @Override
    protected String testName() {
        return "RECALCULATE ILS INSTANCE MQ";
    }
    
    
}
