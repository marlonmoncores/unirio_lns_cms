package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.deterministic.DeterministicSearch;

public class DeterministicTest extends ExperimentBase{
	
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        DeterministicSearch ds = new DeterministicSearch(out, "");
        return ds.execute(mdg);
    }

    @Override
    protected String testName() {
        return "DET";
    }
}

