/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ppgi.unirio.marlon.smc.other.tests;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceParseException;
import br.com.ppgi.unirio.marlon.smc.mdg.ClusterMetrics;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.mdg.simplifier.MDGSimplifier;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.junit.Test;

/**
 * This class recieve a solution and calculate it's MQ Value
 * @author Marlon Monçores
 */
public class SolutionMQCalculator extends ExperimentBase{
    
    private int currentInstanceN = 0;
    private final double errorLimit = 0.0001d;
    private final double errorLimitN = -0.0001d;
    
    public SolutionMQCalculator(){
        this.BEGIN_INSTANCE = 0;//66;//31;
        //this.TOTAL_INSTANCE = 1;
        currentInstanceN = BEGIN_INSTANCE;
    }
    
    @Test
    @Override
    public void runExperiment() throws InstanceParseException, IOException{
        Map<String, SolutionFileData> ILSSolutions = readSolutionsFile("../data/ILS_best_solutions_W_MQ_PROBLEM.csv");
        Map<String, SolutionFileData> LNSSolutions = readSolutionsFile("../data/LNS_best_solutions.csv");
        System.out.println("ALGORITHM;INSTANCE;READ_FILE_VALUE;CALCULATED_VALE;DIFERRENCE_SMALL_THAN_0.0001");
        
        File[] instances = INSTANCE_WORKER.retrieveAllInstanceFiles();//leitura das instancias
        Map<String, ModuleDependencyGraph> mdgs = new HashMap<>();
        for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < TOTAL_INSTANCE;index++){//para cada instancia
            ModuleDependencyGraph mdg = INSTANCE_WORKER.readInstanceFile(instances[index]);
            String name = mdg.getName().toLowerCase();
            mdgs.put(name, mdg);
        }
        
        for (Map.Entry<String, SolutionFileData> entry : ILSSolutions.entrySet()) {
            ModuleDependencyGraph mdg = mdgs.get(entry.getKey());
            if(mdg == null) System.out.println(entry.getKey());
            if(mdg.getName().equalsIgnoreCase("hw")){
                continue;
            }
            calculateMQ("ILS", mdg, entry.getValue().solution, entry.getValue().mq);
        }
        
        for (Map.Entry<String, SolutionFileData> entry : LNSSolutions.entrySet()) {
            ModuleDependencyGraph mdg = mdgs.get(entry.getKey());
            if(mdg == null) System.out.println(entry.getKey());
//            if(mdg.getName().equalsIgnoreCase("hw"))continue;
            MDGSimplifier mDGSimplifier = MDGSimplifier.simplify(mdg);
            mdg = mDGSimplifier.getMdg();
            calculateMQ("LNS", mdg, entry.getValue().solution, entry.getValue().mq);
        }
    }

    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private static class SolutionFileData{
        public String name;
        public int[] solution;
        public double mq;
        
        public SolutionFileData(String name, int[] solution, double mq){
            this.name=name;
            this.solution=solution;
            this.mq=mq;
        }
    }
    
    private Map<String, SolutionFileData> readSolutionsFile(String filePath) throws FileNotFoundException{
        Map<String, SolutionFileData> returnData = new LinkedHashMap<>();
        File file = new File(filePath);
        Scanner in = new Scanner(file);
        if(in.hasNextLine()) in.nextLine(); //discards first line
        
        while(in.hasNextLine()) {
            String line = in.nextLine();
            String[] splitted = line.split(";");
            String name = splitted[0].toLowerCase();
            double mq = Double.parseDouble(splitted[2]);
            String[] solutionT = splitted[1].split(",");
            int[] solution = new int[solutionT.length];
            for(int i=0;i<solutionT.length;i++){
                solution[i] = Integer.parseInt(solutionT[i]);
            }
            returnData.put(name, new SolutionFileData(name, solution, mq));
        }
        
        return returnData;
    }
    
    
    
   
    private void calculateMQ(String algorithm, ModuleDependencyGraph mdg, int[] solution, double oldValue){
        ClusterMetrics cm = new ClusterMetrics(mdg, solution);
        double newMQ = cm.calculateMQ(); 
        double diff = newMQ - oldValue;
        boolean isOK = diff> errorLimitN && diff < errorLimit;
        if(!isOK){
            System.out.println(algorithm+";"+mdg.getName()+";"+oldValue+";"+cm.calculateMQ()+";"+isOK);
        }
    }
 
    

    @Override
    protected String testName() {
        return "RECALCULATE INSTANCE MQ";
    }
    
    
    
}
