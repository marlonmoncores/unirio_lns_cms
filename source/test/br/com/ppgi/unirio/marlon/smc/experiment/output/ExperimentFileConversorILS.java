/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ppgi.unirio.marlon.smc.experiment.output;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import static java.nio.file.Files.newBufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marlon Monçores
 */
public class ExperimentFileConversorILS {
    private static final String PATH="D:\\Marlon\\source_codes\\SMC\\DADOS_ALEXANDRE\\";
    private static final String ALGORITMO="ILS";
    private static final String SEPARADOR=";";
    
    
    
    
    public static List<String> readAllFiles() throws IOException{
        File directory = new File(PATH);
        List<String> outputLines = new ArrayList<>();
        for(File file : directory.listFiles()){
            try (BufferedReader reader = newBufferedReader(file.toPath(), StandardCharsets.ISO_8859_1)) {//abre o arquivo para leitura
                String instance = null;
                for(String line = reader.readLine(); line != null;line = reader.readLine()){
                    if(line.startsWith("INSTÂNCIA=")){
                        instance = line.substring(10, line.length()).trim().toLowerCase();
                    }
                    if(line.startsWith("Cycle")){
                        String[] splited = line.split(";");
                        String mq = splited[2].trim().replace(",", ".").replace("-", "");
                        String time = splited[1].trim();
                        Integer[] group_order = InstanceGroup.INSTANCE_GROUP.get(instance);
                        if(group_order == null){
                            throw new RuntimeException("GRUPO DA INSTANCIA NAO ENCONTRADO. INSTANCIA COM NOME: "+   instance);
                        }
                        String outputLine = instance+SEPARADOR+group_order[0]+SEPARADOR+group_order[1]+SEPARADOR+ALGORITMO+SEPARADOR+mq+SEPARADOR+time;
                        outputLines.add(outputLine);
                    }
                }   
            }
        }
        return outputLines;
    }
            
    
    
    public static void main (String... args) throws IOException{
        ExperimentFileConversorILS.readAllFiles();
    }
    
}
