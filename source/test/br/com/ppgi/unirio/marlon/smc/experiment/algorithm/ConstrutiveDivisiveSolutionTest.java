package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveDivisive;


public class ConstrutiveDivisiveSolutionTest extends ExperimentBase{

	
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        return new ConstrutiveDivisive().createSolution(mdg);
    }
    
    @Override
    protected String testName() {
        return ConstrutiveDivisive.NAME;
    }
}
