/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ppgi.unirio.marlon.smc.experiment.output;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Marlon Monçores
 */
public class InstanceGroup {
    public static final Map<String,Integer[]> INSTANCE_GROUP;
    
    static{
        INSTANCE_GROUP = new HashMap<>();
        INSTANCE_GROUP.put("squid".toLowerCase(), new Integer[]{1, 1});
        INSTANCE_GROUP.put("small".toLowerCase(), new Integer[]{1, 2});
        INSTANCE_GROUP.put("compiler".toLowerCase(), new Integer[]{1, 3});
        INSTANCE_GROUP.put("random".toLowerCase(), new Integer[]{1, 4});
        INSTANCE_GROUP.put("regexp".toLowerCase(), new Integer[]{1, 5});
        INSTANCE_GROUP.put("jstl".toLowerCase(), new Integer[]{1, 6});
        INSTANCE_GROUP.put("lab4".toLowerCase(), new Integer[]{1, 7});
        INSTANCE_GROUP.put("netkit-ping".toLowerCase(), new Integer[]{1, 8});
        INSTANCE_GROUP.put("nss_ldap".toLowerCase(), new Integer[]{1, 9});
        INSTANCE_GROUP.put("nos".toLowerCase(), new Integer[]{1, 10});
        INSTANCE_GROUP.put("lslayout".toLowerCase(), new Integer[]{1, 11});
        INSTANCE_GROUP.put("boxer".toLowerCase(), new Integer[]{1, 12});
        INSTANCE_GROUP.put("netkit-tftpd".toLowerCase(), new Integer[]{1, 13});
        INSTANCE_GROUP.put("sharutils".toLowerCase(), new Integer[]{1, 14});
        INSTANCE_GROUP.put("Mtunis".toLowerCase(), new Integer[]{1, 15});
        INSTANCE_GROUP.put("spdb".toLowerCase(), new Integer[]{1, 16});
        INSTANCE_GROUP.put("xtell".toLowerCase(), new Integer[]{1, 17});
        INSTANCE_GROUP.put("bunch".toLowerCase(), new Integer[]{1, 18});
        INSTANCE_GROUP.put("Ispell".toLowerCase(), new Integer[]{1, 19});
        INSTANCE_GROUP.put("netkit-inetd".toLowerCase(), new Integer[]{1, 20});
        INSTANCE_GROUP.put("Nanoxml".toLowerCase(), new Integer[]{1, 21});
        INSTANCE_GROUP.put("ciald".toLowerCase(), new Integer[]{1, 22});
        INSTANCE_GROUP.put("jodamoney".toLowerCase(), new Integer[]{1, 23});
        INSTANCE_GROUP.put("Modulizer".toLowerCase(), new Integer[]{1, 24});
        INSTANCE_GROUP.put("bootp".toLowerCase(), new Integer[]{1, 25});
        INSTANCE_GROUP.put("jxlsreader".toLowerCase(), new Integer[]{1, 26});
        INSTANCE_GROUP.put("sysklogd-1".toLowerCase(), new Integer[]{1, 27});
        INSTANCE_GROUP.put("telnetd".toLowerCase(), new Integer[]{1, 28});
        INSTANCE_GROUP.put("crond".toLowerCase(), new Integer[]{1, 29});
        INSTANCE_GROUP.put("netkit-ftp".toLowerCase(), new Integer[]{1, 30});
        INSTANCE_GROUP.put("Rcs".toLowerCase(), new Integer[]{1, 31});
        INSTANCE_GROUP.put("seemp".toLowerCase(), new Integer[]{1, 32});
        INSTANCE_GROUP.put("dhcpd-2".toLowerCase(), new Integer[]{1, 33});
        INSTANCE_GROUP.put("cyrus-sasl".toLowerCase(), new Integer[]{1, 34});
        INSTANCE_GROUP.put("tcsh".toLowerCase(), new Integer[]{1, 35});
        INSTANCE_GROUP.put("micq".toLowerCase(), new Integer[]{1, 36});
        INSTANCE_GROUP.put("apache_zip".toLowerCase(), new Integer[]{1, 37});
        INSTANCE_GROUP.put("star".toLowerCase(), new Integer[]{1, 38});
        INSTANCE_GROUP.put("Bison".toLowerCase(), new Integer[]{1, 39});
        INSTANCE_GROUP.put("cia".toLowerCase(), new Integer[]{1, 40});
        INSTANCE_GROUP.put("stunnel".toLowerCase(), new Integer[]{1, 41});
        INSTANCE_GROUP.put("minicom".toLowerCase(), new Integer[]{1, 42});
        INSTANCE_GROUP.put("mailx".toLowerCase(), new Integer[]{1, 43});
        INSTANCE_GROUP.put("dot".toLowerCase(), new Integer[]{1, 44});
        INSTANCE_GROUP.put("screen".toLowerCase(), new Integer[]{1, 45});
        INSTANCE_GROUP.put("slang".toLowerCase(), new Integer[]{1, 46});
        INSTANCE_GROUP.put("slrn".toLowerCase(), new Integer[]{1, 47});
        INSTANCE_GROUP.put("net-tools".toLowerCase(), new Integer[]{1, 48});
        INSTANCE_GROUP.put("graph10up49".toLowerCase(), new Integer[]{1, 49});
        INSTANCE_GROUP.put("wu-ftpd-1".toLowerCase(), new Integer[]{1, 50});
        INSTANCE_GROUP.put("joe".toLowerCase(), new Integer[]{1, 51});
        INSTANCE_GROUP.put("hw".toLowerCase(), new Integer[]{1, 52});
        INSTANCE_GROUP.put("imapd-1".toLowerCase(), new Integer[]{1, 53});
        INSTANCE_GROUP.put("wu-ftpd-3".toLowerCase(), new Integer[]{1, 54});
        INSTANCE_GROUP.put("udt-java".toLowerCase(), new Integer[]{1, 55});
        INSTANCE_GROUP.put("javaocr".toLowerCase(), new Integer[]{1, 56});
        INSTANCE_GROUP.put("dhcpd-1".toLowerCase(), new Integer[]{1, 57});
        INSTANCE_GROUP.put("icecast".toLowerCase(), new Integer[]{1, 58});
        INSTANCE_GROUP.put("pfcda_base".toLowerCase(), new Integer[]{1, 59});
        INSTANCE_GROUP.put("servletapi".toLowerCase(), new Integer[]{1, 60});
        INSTANCE_GROUP.put("php".toLowerCase(), new Integer[]{1, 61});
        INSTANCE_GROUP.put("bunch2".toLowerCase(), new Integer[]{1, 62});
        INSTANCE_GROUP.put("forms".toLowerCase(), new Integer[]{1, 63});
        INSTANCE_GROUP.put("Jscatterplot".toLowerCase(), new Integer[]{1, 64});
        INSTANCE_GROUP.put("jxlscore".toLowerCase(), new Integer[]{2, 65});
        INSTANCE_GROUP.put("elm-2".toLowerCase(), new Integer[]{2, 66});
        INSTANCE_GROUP.put("jfluid".toLowerCase(), new Integer[]{2, 67});
        INSTANCE_GROUP.put("Grappa".toLowerCase(), new Integer[]{2, 68});
        INSTANCE_GROUP.put("elm-1".toLowerCase(), new Integer[]{2, 69});
        INSTANCE_GROUP.put("gnupg".toLowerCase(), new Integer[]{2, 70});
        INSTANCE_GROUP.put("inn".toLowerCase(), new Integer[]{2, 71});
        INSTANCE_GROUP.put("bash".toLowerCase(), new Integer[]{2, 72});
        INSTANCE_GROUP.put("jpassword".toLowerCase(), new Integer[]{2, 73});
        INSTANCE_GROUP.put("bitchx".toLowerCase(), new Integer[]{2, 74});
        INSTANCE_GROUP.put("Junit".toLowerCase(), new Integer[]{2, 75});
        INSTANCE_GROUP.put("xntp".toLowerCase(), new Integer[]{2, 76});
        INSTANCE_GROUP.put("acqCIGNA".toLowerCase(), new Integer[]{2, 77});
        INSTANCE_GROUP.put("bunch_2".toLowerCase(), new Integer[]{2, 78});
        INSTANCE_GROUP.put("exim".toLowerCase(), new Integer[]{2, 79});
        INSTANCE_GROUP.put("xmldom".toLowerCase(), new Integer[]{2, 80});
        INSTANCE_GROUP.put("cia++".toLowerCase(), new Integer[]{2, 81});
        INSTANCE_GROUP.put("Tinytim".toLowerCase(), new Integer[]{2, 82});
        INSTANCE_GROUP.put("mod_ssl".toLowerCase(), new Integer[]{2, 83});
        INSTANCE_GROUP.put("jkaryoscope".toLowerCase(), new Integer[]{2, 84});
        INSTANCE_GROUP.put("ncurses".toLowerCase(), new Integer[]{2, 85});
        INSTANCE_GROUP.put("gae_plugin_core".toLowerCase(), new Integer[]{2, 86});
        INSTANCE_GROUP.put("lynx".toLowerCase(), new Integer[]{2, 87});
        INSTANCE_GROUP.put("javacc".toLowerCase(), new Integer[]{2, 88});
        INSTANCE_GROUP.put("lucent".toLowerCase(), new Integer[]{2, 89});
        INSTANCE_GROUP.put("JavaGeom".toLowerCase(), new Integer[]{2, 90});
        INSTANCE_GROUP.put("Incl".toLowerCase(), new Integer[]{2, 91});
        INSTANCE_GROUP.put("Jdendogram".toLowerCase(), new Integer[]{2, 92});
        INSTANCE_GROUP.put("xmlapi".toLowerCase(), new Integer[]{2, 93});
        INSTANCE_GROUP.put("jmetal".toLowerCase(), new Integer[]{3, 94});
        INSTANCE_GROUP.put("graph10up193".toLowerCase(), new Integer[]{3, 95});
        INSTANCE_GROUP.put("dom4j".toLowerCase(), new Integer[]{3, 96});
        INSTANCE_GROUP.put("nmh".toLowerCase(), new Integer[]{3, 97});
        INSTANCE_GROUP.put("pdf_renderer".toLowerCase(), new Integer[]{3, 98});
        INSTANCE_GROUP.put("Jung_graph_model".toLowerCase(), new Integer[]{3, 99});
        INSTANCE_GROUP.put("jung_visualization".toLowerCase(), new Integer[]{3, 100});
        INSTANCE_GROUP.put("jconsole".toLowerCase(), new Integer[]{3, 101});
        INSTANCE_GROUP.put("pfcda_swing".toLowerCase(), new Integer[]{3, 102});
        INSTANCE_GROUP.put("jml-1.0b4".toLowerCase(), new Integer[]{3, 103});
        INSTANCE_GROUP.put("jpassword2".toLowerCase(), new Integer[]{3, 104});
        INSTANCE_GROUP.put("Notelab-full".toLowerCase(), new Integer[]{3, 105});
        INSTANCE_GROUP.put("Poormans CMS".toLowerCase(), new Integer[]{3, 106});
        INSTANCE_GROUP.put("log4j".toLowerCase(), new Integer[]{3, 107});
        INSTANCE_GROUP.put("jtreeview".toLowerCase(), new Integer[]{3, 108});
        INSTANCE_GROUP.put("bunchall".toLowerCase(), new Integer[]{3, 109});
        INSTANCE_GROUP.put("JACE".toLowerCase(), new Integer[]{3, 110});
        INSTANCE_GROUP.put("javaws".toLowerCase(), new Integer[]{3, 111});
        INSTANCE_GROUP.put("swing".toLowerCase(), new Integer[]{4, 112});
        INSTANCE_GROUP.put("lwjgl-2.8.4".toLowerCase(), new Integer[]{4, 113});
        INSTANCE_GROUP.put("res_cobol".toLowerCase(), new Integer[]{4, 114});
        INSTANCE_GROUP.put("ping_libc".toLowerCase(), new Integer[]{4, 115});
        INSTANCE_GROUP.put("y_base".toLowerCase(), new Integer[]{4, 116});
        INSTANCE_GROUP.put("krb5".toLowerCase(), new Integer[]{4, 117});
        INSTANCE_GROUP.put("apache_ant_taskdef".toLowerCase(), new Integer[]{4, 118});
        INSTANCE_GROUP.put("itextpdf".toLowerCase(), new Integer[]{4, 119});
        INSTANCE_GROUP.put("apache_lucene_core".toLowerCase(), new Integer[]{4, 120});
        INSTANCE_GROUP.put("eclipse_jgit".toLowerCase(), new Integer[]{4, 121});
        INSTANCE_GROUP.put("linux".toLowerCase(), new Integer[]{4, 122});
        INSTANCE_GROUP.put("apache_ant".toLowerCase(), new Integer[]{4, 123});
        INSTANCE_GROUP.put("ylayout".toLowerCase(), new Integer[]{4, 124});
		

    }
}
