package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.experiment.output.ResultWriter;
import br.com.ppgi.unirio.marlon.smc.mdg.ClusterMetrics;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.mdg.simplifier.MDGSimplifier;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.AConstrutiveSolutionBuilder;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.InitialSolutionFactory;
import java.io.IOException;

public class ConstrutiveMQTest extends ExperimentBase{

    

    public ConstrutiveMQTest(){
        RUN_TIMES = 10;//30
        INSTANCES_FOLDER = INSTANCES_FOLDERS[0];
        //BEGIN_INSTANCE = 18;
        //TOTAL_INSTANCE = 1;
        
        try {
            String outPath = testName()+"/"+"001";
            out = ResultWriter.configureResultWriter(OUTPUT_TO,outPath, "ALL");
        } catch (IOException ex) {
        }
    }
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        System.out.println(mdg.getName());
        
        //experimento com a istancia original
        runConstrutiveExperiment(mdg, false);
        
        
        //reduzir a instancia
        MDGSimplifier mDGSimplifier = MDGSimplifier.simplify(mdg);
        mDGSimplifier.getMdg();
        
        //experimento com a instancia reduzida
        runConstrutiveExperiment(mdg, true);
        
        return null;
    }
    
    private void runConstrutiveExperiment(ModuleDependencyGraph mdg, boolean reduced){
        for(int execution=0;execution<RUN_TIMES;execution++){
            AConstrutiveSolutionBuilder solutionBuilder = InitialSolutionFactory.build(InitialSolutionFactory.CREATION_METHOD.CAMQ);
            long currentTime = System.currentTimeMillis();
            int[] solution = solutionBuilder.createSolution(mdg);
            long timeTaken = System.currentTimeMillis()-currentTime;
            ClusterMetrics cm = new ClusterMetrics(mdg, solution);
            saveSearchStatus(out, mdg, cm, timeTaken, reduced);
        }
    }
    
    private void saveSearchStatus(ResultWriter out, ModuleDependencyGraph mdg, ClusterMetrics cm, long time, boolean reduced){
        String status;
        if(reduced){
            status="REDUCED";
        }else{
             status="NORMAL";
        }
        out.writeLine(mdg.getName(), status, mdg.getSize()+"", mdg.getTotalDependencyCount()+"", mdg.getTotalDependencyEdgeCount()+"", cm.calculateMQ()+"", time+"");
    }
    
  
    @Override
    protected void afterAll(){
        out.close();
    }
    @Override
    protected final String testName() {
        return "ConstrutiveMQComparator";
    }

}
