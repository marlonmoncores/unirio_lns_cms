/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ppgi.unirio.marlon.smc.experiment.output;

import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceWriteException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Marlon Monçores
 */
public class MergeExperimentFiles {
    private static final String PATH="D:\\Marlon\\source_codes\\SMC\\experiment_dissertacao\\MERGED_DATA\\";
    private static final String FILE_NAME="ils_lns_data.dat";
    
    
    
    
    public static void mergeFiles() throws IOException, InstanceWriteException{
        try{
            File file = new File(PATH+FILE_NAME);
            if(file.exists()){
                    throw new InstanceWriteException("OUTPUT FILE ALREADY EXISTS, OPERATION WAS CANCELED!");
            }
            file.createNewFile();

            PrintWriter  pw = new PrintWriter(PATH+FILE_NAME);		
            pw.println("INSTANCE;GROUP;ORDER;SOLVER;MQ;TIME");

            for (String data : ExperimentFileConversorILS.readAllFiles()) {
                pw.println(data);
            }
            for (String data : ExperimentFileConversorLNS.readAllFiles()) {
                pw.println(data);
            }
            pw.close();
            
        }catch(IOException ioe){
            throw new InstanceWriteException(ioe);
        }
    }
            
    
    
    public static void main (String... args) throws IOException, InstanceWriteException{
        MergeExperimentFiles.mergeFiles();
    }
    
}
