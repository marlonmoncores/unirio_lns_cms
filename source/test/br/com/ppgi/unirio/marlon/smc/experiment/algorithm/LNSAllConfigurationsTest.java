package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.experiment.output.ResultWriter;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.mdg.simplifier.MDGSimplifier;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.hc.HillClimbing;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LNSConfiguration;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LNSConfigurationBase;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LNSConfigurationBuilder10ConfigsManyRepairs;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LargeNeighborhoodSearch;
import java.io.IOException;

public class LNSAllConfigurationsTest extends ExperimentBase{

    private ResultWriter hcOut;
    
    public LNSAllConfigurationsTest(){
        RUN_TIMES = 1;
    }
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        int sizeBefore = mdg.getSize();
        MDGSimplifier.simplify(mdg);//simplificar MDG
        System.out.println("TAMANHO DE: "+sizeBefore+" PARA: "+mdg.getSize());
        //LNSConfigurationBuilder configurationBuilder = new LNSConfigurationBuilderMultiply();
        LNSConfigurationBase configurationBuilder = new LNSConfigurationBuilder10ConfigsManyRepairs();
        for (int configN=0;configN<configurationBuilder.calculateTotalConfigurationsAvailable();configN++){
            String fileName = configN+"_all";
            String outPath = beginTestTimestamp+"/"+mdg.getName();
            LNSConfiguration config = configurationBuilder.buildConfiguration(configN, mdg, OUTPUT_TO,outPath,fileName);
            hcOut = null;
            for (int i = 1; i <= RUN_TIMES; i++) {//para cada vez desejada
                long time = System.currentTimeMillis();

                LargeNeighborhoodSearch lns = new LargeNeighborhoodSearch(config);
                int[] lnsSolutionFound = lns.execute();//executa o algoritmo

                time = System.currentTimeMillis()-time;
                System.out.println(mdg.getName()+";"+config.toString()+";"+i+";"+time);

                if(hcOut == null){
                    hcOut = runAfterTest(mdg, lnsSolutionFound, outPath, fileName, config.toString());
                }else{
                    runAfterTest(mdg, lnsSolutionFound, config.toString(), hcOut);
                }

                if(i< RUN_TIMES){//nao roda na ultima vez
                    afterEachTime();
                }
            }
            config.close();
            hcOut.close();
        }
        return null;
    }
    
    
    private ResultWriter runAfterTest(ModuleDependencyGraph mdg, int[] initialSolution, String outPath, String fileName, String params){
        try{
            hcOut = ResultWriter.configureResultWriter(OUTPUT_TO,outPath, "HC"+"_"+mdg.getName()+"_"+fileName);
            runAfterTest(mdg, initialSolution, params, hcOut);
            return hcOut;
        }catch(IOException ioe){
            
        }   
        return null;
    }
    
    private void runAfterTest(ModuleDependencyGraph mdg, int[] initialSolution, String params, ResultWriter hcOut){
        HillClimbing hc = new HillClimbing(hcOut, "", params);
        hc.execute(mdg, initialSolution, HillClimbingTest.THRESHOLD);
    }
    
    
    @Override
    protected String testName() {
        return "LNS";
    }

}
