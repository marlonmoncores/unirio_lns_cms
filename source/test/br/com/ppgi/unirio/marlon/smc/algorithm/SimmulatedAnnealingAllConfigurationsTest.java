package br.com.ppgi.unirio.marlon.smc.algorithm;

import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.sa.SimmulatedAnnealingTemperature;
import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.experiment.output.ResultWriter;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.AConstrutiveSolutionBuilder;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LNSConfiguration;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LNSConfigurationBase;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.lns.LNSConfigurationBuilderMultiply;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimmulatedAnnealingAllConfigurationsTest extends ExperimentBase{

    public SimmulatedAnnealingAllConfigurationsTest(){
    }
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        try {
            String outPath = beginTestTimestamp+"/"+mdg.getName();
            out = ResultWriter.configureResultWriter(OUTPUT_TO,outPath, "SA"+"_"+mdg.getName());
        } catch (IOException ex) {
        }
        LNSConfigurationBase configurationBase = new LNSConfigurationBuilderMultiply();
        List<String> usedConfigs = new ArrayList<>();
        System.out.println(mdg.getName());
        for (int configN=0;configN<configurationBase.calculateTotalConfigurationsAvailable();configN++){
            LNSConfiguration config = configurationBase.buildConfiguration(configN, mdg);
            
            
            
            AConstrutiveSolutionBuilder solutionBuilder = config.getInitialSolutionBuilder();
            float coolingRate = config.getCoolingRate();
            float initialTemperatureRatio = config.getInicialTemperatureRatio();
            int iterations = config.getIterationLimit();

            boolean canUse = addConfigurationToUsedList(usedConfigs, solutionBuilder, coolingRate, initialTemperatureRatio, iterations);
            if(canUse){
                SimmulatedAnnealingTemperature sat = new SimmulatedAnnealingTemperature(out);
                sat.execute(mdg, solutionBuilder, coolingRate, initialTemperatureRatio, iterations);
            }
            config.close();
        }
        out.close();
        return null;
    }
    
    /**
     * adiciona a configuração na lista de configs utilizada.
     * retorna uma booleana informando se a config foi adicionada ou não (retorna false, se já existir)
     * @param configsUsed
     * @param solutionBuilder
     * @param coolingRate
     * @param initialTemperatureRatio
     * @param iterations
     * @return 
     */
    private boolean addConfigurationToUsedList(List<String> configsUsed, AConstrutiveSolutionBuilder solutionBuilder, float coolingRate, float initialTemperatureRatio, int iterations){
        String newConfig = solutionBuilder.getName()+coolingRate+initialTemperatureRatio+iterations;
    
        if(configsUsed.contains(newConfig)){
            return false;
        }else{
            configsUsed.add(newConfig);
            return true;
        }
    }

    @Override
    protected String testName() {
        return "SA";
    }

}
