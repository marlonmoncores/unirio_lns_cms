/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ppgi.unirio.marlon.smc.other.tests;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceParseException;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.mdg.graph.DepthFirstSearch;
import br.com.ppgi.unirio.marlon.smc.mdg.simplifier.MDGSimplifier;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author Marlon Monçores
 */
public class GraphIndependentComponentTest extends ExperimentBase{

    
    @Test
    @Override
    public void runExperiment() throws InstanceParseException, IOException{
        File[] instances = INSTANCE_WORKER.retrieveAllInstanceFiles();//leitura das instancias
        System.out.println("INSTÂNCIA;TAMANHO;COMPONENTES;[CABEÇA;TAMANHO];[CABEÇA;TAMANHO];[CABEÇA;TAMANHO];...;...");
        for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < TOTAL_INSTANCE;index++){//para cada instancia
            ModuleDependencyGraph mdg = INSTANCE_WORKER.readInstanceFile(instances[index]);
            runAlgorithm(mdg);
        }
    }
    
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        MDGSimplifier simpled = MDGSimplifier.simplify(mdg);
        Map<String,List<Integer>> data = DepthFirstSearch.doSearch(simpled.getMdg());
        List<Integer> componentHead = data.get("HEAD");
        List<Integer> componentSize = data.get("SIZE");
        
        System.out.print(mdg.getName()+";"+mdg.getSize()+";"+componentHead.size());
        
        for(int i=0;i<componentHead.size();i++){//para cada componente conexa
            int head = componentHead.get(i);
            int size = componentSize.get(i);
            System.out.print(";"+head+";"+size);
        }
        System.out.println("");
        return null;
    }

    @Override
    protected String testName() {
        return "Exibir configurações das instâncias";
    }
    
    
    
}
