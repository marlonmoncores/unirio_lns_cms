/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.ppgi.unirio.marlon.smc.other.tests;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceParseException;
import br.com.ppgi.unirio.marlon.smc.mdg.ClusterMetrics;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveBasicOneModulePerCluster;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveBasicRandomSolution;
import java.io.File;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import random.number.generator.RandomWrapper;

/**
 *
 * @author Marlon Monçores
 */
public class MQCalculatorTest extends ExperimentBase{
    
    private final int TEST_LIMIT_MOVES=1000;
    private int currentInstanceN = 0;
    
    public MQCalculatorTest(){
        this.BEGIN_INSTANCE = 0;//66;//31;
        //this.TOTAL_INSTANCE = 1;
        currentInstanceN = BEGIN_INSTANCE;
    }
    
    @Test
    @Override
    public void runExperiment() throws InstanceParseException, IOException{
        File[] instances = INSTANCE_WORKER.retrieveAllInstanceFiles();//leitura das instancias
        for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < TOTAL_INSTANCE;index++){//para cada instancia
            ModuleDependencyGraph mdg = INSTANCE_WORKER.readInstanceFile(instances[index]);
            runAlgorithm(mdg);
        }
    }
    
    
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        System.out.println("TESTING INSTANCE "+(currentInstanceN++) +" "+mdg.getName()+" ... ");
        
        if(mdg.getName().equalsIgnoreCase("06-small")){
            test06SmallInstance(mdg);
        }
        testManyMoviments(mdg);
        
        return null;
    }
    
    private void testManyMoviments(ModuleDependencyGraph mdg){
        double errorLimit = 0.001d;
        int[] initialSolution = new ConstrutiveBasicRandomSolution().createSolution(mdg);
        
        for( int i=0;i<TEST_LIMIT_MOVES;i++){
            ClusterMetrics initialCM = new ClusterMetrics(mdg, initialSolution);
            double initialMQ = initialCM.calculateMQ();

            int moveModule = (int)RandomWrapper.unif(0, mdg.getSize()-1);
            int toCluster = (int)RandomWrapper.unif(0, mdg.getSize()-1);

            double calculatedDelta = initialCM.calculateMovimentDelta(moveModule, toCluster);
            initialCM.makeMoviment(moveModule, toCluster);
            double newMQ = initialCM.calculateMQ();

            int[] finalSolution = initialCM.cloneSolution();
            ClusterMetrics finalCM = new ClusterMetrics(mdg, finalSolution);
            double finalMQ = finalCM.calculateMQ();
            double finalDelta = finalMQ - initialMQ;

            try{
                Assert.assertEquals("DELTA DEVERIA SER: ", finalDelta, calculatedDelta, errorLimit);
                Assert.assertEquals("MQ DEVERIA SER: ", finalMQ, newMQ, errorLimit);
            }catch(Error afe){
                throw afe;
            }
        }
        
        
    }
    private void test06SmallInstance(ModuleDependencyGraph mdg){
        double errorLimit = 0.001d;
        
        int[] solution = new int[]{0,0,0,0,1,1};
        ClusterMetrics cm = new ClusterMetrics(mdg, solution);
        double expectedValue = 1.2063492063;
        System.out.println("CLUSTER;I;J");
        System.out.println(cm.getClustersStatusAsString());
        Assert.assertEquals("VALOR INCORRETO!", expectedValue, cm.calculateMQ(),errorLimit);
        
        solution = new int[]{0,0,0,2,1,1};
        cm = new ClusterMetrics(mdg, solution);
        expectedValue = 1.1503267974;
        System.out.println(cm.getClustersStatusAsString());
        Assert.assertEquals("VALOR INCORRETO!", expectedValue, cm.calculateMQ(),errorLimit);
        
        solution = new int[]{0,0,2,2,1,1};
        cm = new ClusterMetrics(mdg, solution);
        expectedValue = 1.5598290598;
        System.out.println(cm.getClustersStatusAsString());
        Assert.assertEquals("VALOR INCORRETO!", expectedValue, cm.calculateMQ(),errorLimit);
        
        solution = new int[]{3,1,5,5,3,0};
        cm = new ClusterMetrics(mdg, solution);
        expectedValue = 0.6153846154;
        System.out.println(cm.getClustersStatusAsString());
        Assert.assertEquals("VALOR INCORRETO!", expectedValue, cm.calculateMQ(),errorLimit);
        
        solution = new int[]{3,1,1,5,3,0};
        cm = new ClusterMetrics(mdg, solution);
        expectedValue = 0.5333333333;
        System.out.println(cm.getClustersStatusAsString());
        Assert.assertEquals("VALOR INCORRETO!", expectedValue, cm.calculateMQ(),errorLimit);

        
        test06SmallMoviments(mdg);
        
    }
    
    private void test06SmallMoviments(ModuleDependencyGraph mdg){
        double errorLimit = 0.001d;
        
        int[] solution = new int[]{0,0,0,0,1,1};
        ClusterMetrics cm = new ClusterMetrics(mdg, solution);
        double inicialValue = 1.2063492063;
        double finalValue = 1.1503267974;
        double expectedDelta = finalValue - inicialValue;
        
        double readDelta = cm.calculateMovimentDelta(3, 2);
        Assert.assertEquals("DELTA INCORRETO!", expectedDelta, readDelta, errorLimit);
    }
    
    

    @Override
    protected String testName() {
        return "Exibir configurações das instâncias";
    }
    
    
    
}
