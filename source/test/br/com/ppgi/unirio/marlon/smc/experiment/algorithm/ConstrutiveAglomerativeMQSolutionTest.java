package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveAglomerativeMQ;


public class ConstrutiveAglomerativeMQSolutionTest extends ExperimentBase{

	
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        return new ConstrutiveAglomerativeMQ().createSolution(mdg);
    }
    
    @Override
    protected String testName() {
        return ConstrutiveAglomerativeMQ.NAME;
    }
}
