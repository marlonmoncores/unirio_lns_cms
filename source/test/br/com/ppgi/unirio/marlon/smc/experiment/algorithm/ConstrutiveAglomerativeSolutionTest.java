package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.construtive.ConstrutiveAglomerative;


public class ConstrutiveAglomerativeSolutionTest extends ExperimentBase{

	
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        return new ConstrutiveAglomerative().createSolution(mdg);
    }
    
    @Override
    protected String testName() {
        return ConstrutiveAglomerative.NAME;
    }
}
