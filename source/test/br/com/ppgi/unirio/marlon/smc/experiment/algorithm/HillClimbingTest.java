package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.hc.HillClimbing;

public class HillClimbingTest extends ExperimentBase{
	
    public static final int POP_SZ = 100;/* quantidade de soluções aleatórias que serão geradas */
    public static final int THRESHOLD = 70; /* % de vizinhos que será analisado antes de decidir para qual vizinho a busca irá */
    
            
    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        HillClimbing hc = new HillClimbing(out, "");
        return hc.execute(mdg, POP_SZ, THRESHOLD);
    }

    @Override
    protected String testName() {
        return "HC";
    }
}
