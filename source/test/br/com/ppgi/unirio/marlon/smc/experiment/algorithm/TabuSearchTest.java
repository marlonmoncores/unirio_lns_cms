package br.com.ppgi.unirio.marlon.smc.experiment.algorithm;

import br.com.ppgi.unirio.marlon.smc.experiment.ExperimentBase;
import br.com.ppgi.unirio.marlon.smc.experiment.output.ResultWriter;
import br.com.ppgi.unirio.marlon.smc.instance.graph.GraphViz;
import br.com.ppgi.unirio.marlon.smc.mdg.ClusterMetrics;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.ts.TabuSearch;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.ts.configuration.TabuConfiguration;
import br.com.ppgi.unirio.marlon.smc.solution.algorithm.heuristic.ts.configuration.TabuConfigurationManager;
import java.io.IOException;
import java.util.List;

public class TabuSearchTest extends ExperimentBase{

    private static final int TABU_RESTARTS = 1;

    @Override
    protected int[] runAlgorithm(ModuleDependencyGraph mdg) {
        
        int[] bestSolution = null;
        double bestSolutionMQ = Integer.MIN_VALUE;
        
        //rodar a instancia com todas as vairacoes de configuracao
        TabuConfigurationManager config = new TabuConfigurationManager(out);
        List<TabuConfiguration> configurations = config.createDefaultConfigurations(mdg.getSize(),TABU_RESTARTS);
        for(TabuConfiguration currentConfiguration : configurations){
            //HillClimbing hc = new HillClimbing(out, "");
            //int[] initialSolution = hc.execute(mdg, HillClimbingTest.POP_SZ, HillClimbingTest.THRESHOLD);
            TabuSearch ts = currentConfiguration.getTabuSearch();
            
            //int[] solution = ts.execute(mdg, initialSolution, currentConfiguration);
            int[] solution = ts.execute(mdg, currentConfiguration);
            
            //gerar arquivo com gráfico
            try {
                ResultWriter graphOut = ResultWriter.configureResultWriter(OUTPUT_TO,mdg.getName()+"_"+currentConfiguration.toString(),"graph");
                graphOut.writeGraph(GraphViz.generateGraphCode(mdg, solution));
                graphOut.close();
            } catch (IOException ex) {
            }
            
            ClusterMetrics cm = new ClusterMetrics(mdg,solution);
            if(cm.calculateMQ() > bestSolutionMQ){
                bestSolution = cm.cloneSolution();
                bestSolutionMQ = cm.calculateMQ();
            }
            
        }
        return bestSolution;
    }

    @Override
    protected String testName() {
        return "TS";
    }

}
