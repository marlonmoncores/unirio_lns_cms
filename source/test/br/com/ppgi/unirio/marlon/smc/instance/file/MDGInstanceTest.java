package br.com.ppgi.unirio.marlon.smc.instance.file;
import br.com.ppgi.unirio.marlon.smc.instance.file.mdg.MDGInstanceFileWorker;
import br.com.ppgi.unirio.marlon.smc.instance.file.odem.OdemInstanceFileWorker;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;
import java.io.File;
import org.junit.Test;

public class MDGInstanceTest{

	private MDGInstanceFileWorker instanceWorker = new MDGInstanceFileWorker();
        
        /**
         * Cria as instâncias do tipo MDG a partir das inst�ncias ODEM
         * @throws InstanceWriteException
         * @throws InstanceParseException 
         */
	@Test
	public void createMDGFiles() throws InstanceWriteException, InstanceParseException{
		OdemInstanceFileWorker odemInstanceFileWorker = new OdemInstanceFileWorker();
		File[] instanceFiles = odemInstanceFileWorker.retrieveAllInstanceFiles();
		
		for(File currentInstance : instanceFiles){
                    try{
                        instanceWorker.writeInstanceFile(odemInstanceFileWorker.readInstanceFile(currentInstance));
                    }catch(InstanceWriteException iwe){
                        System.out.println("INSTÂNCIA JÁ EXISTE ... IGNORANDO");
                    }
		}
	}
        
        /**
         * Executa a leitura de todas as instancias do tipo mdg
         * @throws InstanceParseException 
         */
	@Test
	public void runAllInstances () throws InstanceParseException{
		File[] instanceFiles = instanceWorker.retrieveAllInstanceFiles();
					
		for(File currentInstance: instanceFiles){
			readInstanceFile(currentInstance);
		}		
	}
        
        /**
         * Efetua a leitura de uma instancia no formato mdg e escreve no console alguns dados sobre a mesma
         * @param currentInstance
         * @throws InstanceParseException 
         */
        private void readInstanceFile(File currentInstance) throws InstanceParseException {
		System.out.println("INSTANCE START: "+ currentInstance.getName());
		
		ModuleDependencyGraph mdg = instanceWorker.readInstanceFile(currentInstance);
		
		System.out.println("INSTANCE NAME: "+mdg.getName());
		System.out.println("INSTANCE PACKAGES COUNT: N/A");
		System.out.println("INSTANCE CLASSES COUNT: "+mdg.getSize());
		System.out.println("INSTANCE DEPENDENCY COUNT: "+mdg.getTotalDependencyCount());
                
		System.out.println("INSTANCE END: "+ currentInstance.getName());
                System.out.println("------------- ---------------- ---------------");
	}
}
