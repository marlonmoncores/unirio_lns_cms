package OLD;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import br.com.ppgi.unirio.marlon.smc.instance.file.InstanceParseException;
import br.com.ppgi.unirio.marlon.smc.instance.graph.GraphViz;
import br.com.ppgi.unirio.marlon.smc.instance.file.mdg.MDGInstanceFileWorker;
import br.com.ppgi.unirio.marlon.smc.instance.graph.GraphViz;
import br.com.ppgi.unirio.marlon.smc.mdg.ModuleDependencyGraph;

public class GraphTest {
	private final int BEGIN_INSTANCE = 0;
        private final int TOTAL_INSTANCE = 1;//Integer.MAX_VALUE;
	
        @Test
	public void runTest() throws InstanceParseException, IOException{
            MDGInstanceFileWorker instanceWorker = new MDGInstanceFileWorker();
            File[] instances = instanceWorker.retrieveAllInstanceFiles();//leitura das instancias
            //para cada instancia
            for(int index=BEGIN_INSTANCE;index<instances.length && index-BEGIN_INSTANCE < TOTAL_INSTANCE;index++){
                ModuleDependencyGraph currentInstanceMDG = instanceWorker.readInstanceFile(instances[index]);
                System.out.println("INSTANCE: "+currentInstanceMDG.getName());

                StringBuilder graph = GraphViz.generateGraphCode(currentInstanceMDG);
                System.out.println(graph);
            }
	}
}

